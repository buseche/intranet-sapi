<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Perfil de Usuario</h6>
        </div>
        <div class="card-body">
            <!--Navigatior-->
            <form method="post" class="">
                <div id="personal_data">
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Nombre</label>
                        <div class="col-md-6">
                            <input class="form-control" id="nombre" name="nombre" disabled value="<?php echo $nombre;?>">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Apellido</label>
                        <div class="col-md-6">
                            <input class="form-control" id="apellido" name="apellido" disabled value="<?php echo $apellido;?>">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Cedula</label>
                        <div class="col-md-6">
                            <input class="form-control" id="cedula" name="cedula" disabled value="<?php echo $cedula;?>">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Correo Electronico</label>
                        <div class="col-md-6">
                            <input class="form-control" id="email" name="email" disabled value="<?php echo $email;?>">
                        </div>
                    </div>
                </div>
                <div id="work_data">
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Codigo Personal</label>
                        <div class="col-md-6">
                            <input class="form-control" id="nombre" name="nombre" disabled value="<?php echo $id_user;?>">
                        </div>
                    </div>

                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Unidad Administrativa</label>
                        <div class="col-md-6">
                            <input class="form-control" id="uniadm" name="uniadm" disabled value="<?php echo $uniadm;?>">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Fecha Ingreso</label>
                        <div class="col-md-6">
                            <input class="form-control" id="fecing" name="fecing" disabled value="<?php echo $fecing;?>">
                        </div>
                    </div>
                </div>
                <div id="account_data">
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Contraseña</label>
                        <div class="col-md-6">
                            <input class="form-control" type="password" id="password" name="password">
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <label class="col-form-label col-md-3">Confirmar Contraseña</label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" id="confirmpass" name="confirmpass">
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-info form-control" type="button" id="cambiar_clave">Cambiar</button>
                        </div>
                    </div>
                    
                </div>
                <nav>
                    <ul class="pagination justify-content-center pagination-sm flex-wrap">
                        <li class="page-item"><a class="page-link" href="#personal_data">Datos personales</a></li>
                        <li class="page-item"><a class="page-link" href="#work_data">Datos Laborales</a></li>
                        <li class="page-item"><a class="page-link" href="#account_data">Seguridad</a></li>
                    </ul>
                </nav>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->