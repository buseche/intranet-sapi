        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Usuarios registrados en la Intranet</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="users_table" width="100%" cellspacing="0">
                  <?php echo $table;?>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyleft 2020, Servicio Autonomo de la Propiedad Intelectual</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Salir del Sistema</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">¿Está seguro de terminar la sesion?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="<?php echo base_url();?>index.php/login/cerrarSesion">Salir</a>
        </div>
      </div>
    </div>
  </div>

  <!--Modal de edicion de usuario-->
  <div class="modal fade" id="edicionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edicion de informacion del Usuario</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form class="form" method="POST" id="editUser" enctype="multipart/form-data">
          <div class="modal-body">  
            <input type="hidden" name="cedula" id="cedula">
                <div class="form-group">
                    <label for="supervisor">Supervisor</label>
                    <select class="form-control" id="supervisor" name="supervisor">
                        <option value="f">Desactivado</option>
                        <option value="t">Activado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="estatus">Estado del Usuario</label>
                    <select class="form-control" id="estatus" name="estatus">
                        <option value="0">Desactivado</option>
                        <option value="1">Activo</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="pass">Contraseña</label>
                    <input type="password" class="form-control" id="pass" name="pass">
                
                </div>
                <div class="form-group">
                    <label for="Avatar">Foto de Perfil</label>
                    <input type="file" class="form-group" id="avatar" name="avatar">
                    <img id="picProfile" style="width:100px; heigth:100px" src="<?php echo base_url();?>theme/img/perfil.svg" class="img-circle rounded">
                </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <button id="save" class="btn btn-primary" type="submit">Guardar cambios</button>
        </div>
      </form>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>theme/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url();?>theme/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <!--PNotify-->
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotify.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotifyButtons.js"></script>
  <!--Global ENV-->
  <script src="<?php echo base_url();?>theme/js/env_var.js"></script>
  <!-- Page level custom scripts -->
  <script src="<?php echo base_url();?>theme/js/users.js"></script>

</body>

</html>