<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Intranet SAPI">
  <meta name="author" content="Bryan Useche">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?php echo $titulo;?></title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- bootstrap-daterangepicker -->
  <link href="<?php echo base_url();?>theme/vendor/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap datatables-->
  <link href="<?php echo base_url();?>theme/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!--PNotify-->
  <link href="<?php echo base_url();?>node_modules/pnotify/dist/PNotifyBrightTheme.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>node_modules/material-design-icons/iconfont/material-icons.css" />
  <!--AnimateCSS-->
  <link href="<?php echo base_url();?>node_modules/animate.css/animate.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>theme/css/sb-admin-2.css" rel="stylesheet">
  <body>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- 404 Error Text -->
          <div class="text-center">
            <div class="error mx-auto">503</div>
            <p class="text-gray-500 mb-0">Sistema no disponible</p>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyleft 2020. Servicio Autonomo de la Propiedad Intelectual</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Salir del Sistema</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">¿Está seguro de terminar la sesion?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="<?php echo base_url();?>index.php/login/cerrarSesion">Salir</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- bootstrap-daterangepicker -->
  <script src="<?php echo base_url();?>theme/vendor/moment/min/moment.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>theme/js/sb-admin-2.min.js"></script>
  <!--Script especiales-->
  <script src="<?php echo base_url();?>theme/js/env_var.js"></script>
  <script src="<?php echo base_url();?>theme/js/recibos.js"></script>
  

</body>

</html>
