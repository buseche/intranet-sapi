<body class="bg-gradient-info">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Recuperar clave</h1>
                  </div>
                  <!--Form para la cedula del usuario-->
                  <form class="user" id="recover_frm">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="cedula" placeholder="Introduzca su cedula de identidad">
                    </div>
                    <button id="btnGenerar" type="submit" class="btn btn-primary btn-user btn-block">
                      Recuperar
                    </button>
                    <br />
                    <p class="small text-center">Recibiras un correo con un codigo de confirmacion que deberás introducir a continuacion</p>
                    </a>
                  </form>
                  <!--Form para el codigo de confirmacion-->
                  <form class="user" id="confirm_code">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="codigo" placeholder="Introduzca el codigo de confirmacion">
                    </div>
                    <button id="btnConfirmar" type="submit" class="btn btn-primary btn-user btn-block">
                      Confirmar Codigo
                    </button>
                    <br />
                    <p class="small text-center">Si no recibes el código, ponte en contacto con Gestion Humana para la actualizacion de tus datos</p>
                    </a>
                  </form>
                  <!--Form para confirmar la contraseña-->
                  <form class="user" id="confirm_pass">
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="pass1" placeholder="Introduzca nueva contraseña">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="pass2" placeholder="Confirme nuevamente la contraseña">
                    </div>
                    <button id="btnChangePass" type="submit" class="btn btn-primary btn-user btn-block">
                      Restaurar Contraseña
                    </button>
                    <br />
                    <p class="small text-center">La contraseña debe contener al menos una letra, no puede ser tu cedula ni contraseñas genericas como 12345 ó 123456</p>
                    </a>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="<?php echo base_url();?>index.php">Iniciar Sesion</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
   <!-- Confirm Modal-->
   <div class="modal fade" id="confirmCambio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cambio de Contraseña Exitoso</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Tu Contraseña fue cambiada exitosamente</div>
        <div class="modal-footer">
          <a class="btn btn-primary" href="<?php echo base_url();?>">Aceptar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>theme/js/sb-admin-2.js"></script>
  <!--PNotify-->
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotify.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotifyButtons.js"></script>
  <!--Global ENV-->
  <script src="<?php echo base_url();?>theme/js/env_var.js"></script>
  <!--Script para el login-->
  <script src="<?php echo base_url();?>theme/js/login.js"></script>
  <!--reCaptcha V2-->
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>


</body>

</html>