<body class="bg-login-color">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Bienvenido</h1>
                  </div>
                  <form class="user" id="login_frm">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="cedula" placeholder="Introduzca su cedula de identidad">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="clave" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                      <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Entrar
                    </button>
                    </a>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="<?php echo base_url();?>index.php/recuperar">¿Olvidó su clave?</a>
                  </div>
                  <div class="text-center">
                    <p class="small">Si es la primera vez que vas a ingresar, solo coloca tu cédula dos veces</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!--PNotify-->
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotify.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotifyButtons.js"></script>
  <!--Global ENV-->
  <script src="<?php echo base_url();?>theme/js/env_var.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>theme/js/sb-admin-2.js"></script>
  <!--Script para el login-->
  <script src="<?php echo base_url();?>theme/js/login.js"></script>
  <!--Recaptcha V3-->
  <script src="https://www.google.com/recaptcha/api.js?render=6LecrNUUAAAAAEwib6RrDklzMiUEiP3Rv5CSuYS9"></script>
  <script>
  grecaptcha.ready(function() {
    grecaptcha.execute('6LecrNUUAAAAAEwib6RrDklzMiUEiP3Rv5CSuYS9', {action: 'login'}).then(function(token) {
      document.getElementById('g-recaptcha-response').value=token;
    });
  });
  </script>
  <!-- Input Oculto del recaptcha V3-->
  

</body>

</html>