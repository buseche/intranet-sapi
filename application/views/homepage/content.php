<div class="my-3 my-md-5">
  <div class="container">
    <div class="page-header">
      <h1 class="page-title">
        Pagina Principal
      </h1>
    </div>
    <!--Widgets de las noticias-->
    <div class="row row-cards row-deck">
      <!--Noticias del blog-->
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Últimas publicaciones</h3>
          </div>
          <div class="card-body">
            <?php echo $noticias_int;?>
          </div>
        </div>
      </div>
      <!--Noticias de la Pagina-->
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Noticias Institucionales</h3>
          </div>
          <div class="card-body">
            <?php echo $noticias_pag;?>
          </div>
        </div>
      </div>
      </div>
      <!--Cumpleañeros del mes-->
      <div class="row row-cards row-deck">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cumpleañeros del Mes</h3>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <?php echo $birthdays;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>