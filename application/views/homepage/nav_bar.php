<body class="">
  <div class="page">
    <div class="page-main">
      <div class="header py-4">
        <div class="container">
          <div class="d-flex">
            <a class="header-brand" href="<?php echo base_url();?>">
              <img src="<?php echo base_url();?>theme/img/Logosapi-2020.png" class="header-brand-img" alt="SAPI logo" style="height: 38px; width:110px">
            </a>
            <!-- Menu para el Usuario-->
            <div class="d-flex order-lg-2 ml-auto">
              <div class="dropdown d-none d-md-flex">
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <!--Avatar de la persona-->
                    <?php if(is_null($this->session->userdata('avatar'))){?>
                    <span class="avatar" style="background-image: url(<?php echo base_url();?>theme/img/perfil.svg)"></span>
                    <?php } else {?>
                    <span class="avatar" style="background-image: url(<?php echo $this->session->userdata('avatar');?>)"></span>
                    <?php } ?>
                    <span class="ml-2 d-none d-lg-block">
                      <?php if(is_null($this->session->userdata('nombre'))){?>
                      <span class="text-default">Iniciar sesion</span>
                      <?php } else{ ?>
                      <span class="text-default"><?php echo ucwords(strtolower($this->session->userdata('nombre')));?></span>
                      <small class="text-muted d-block mt-1"><?php echo ucwords(strtolower($this->session->userdata('apellido'))); }?></small>
                    </span>
                  </a>
                  <?php if(is_null($this->session->userdata('logged'))){?>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <div class="dropdown-form">
                        <form id="login_frm" method="POST" class="p-2">
                          <div class="form-group">
                            <input type="text" class="form-control form-control-user" id="cedula" placeholder="Cédula de identidad">
                          </div>
                          <div class="form-group">
                            <input type="password" class="form-control form-control-user" id="clave" placeholder="Contraseña">
                          </div>
                          <div class="form-group">
                            <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                          </div>
                          <div class="form-group">
                            <p class="text-mutted"><small>Si es la primera vez que ingresas, solo coloca tu cédula dos veces</small></p>
                          </div>
                          <button type="submit" class="btn btn-primary btn-user btn-block">
                            Entrar
                          </button>
                          <div class="form-group">
                            <a class="small" href="<?php echo base_url();?>index.php/recuperar">¿Olvidó su clave?</a>
                          </div>
                        </form>
                        
                      </div>
                    </div>
                  <?php } else {?>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <a class="dropdown-item" href="<?php echo base_url();?>index.php/perfil">
                        <i class="dropdown-icon fe fe-user"></i> Perfil
                      </a>
                      <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                        <i class="dropdown-icon fe fe-log-out"></i> Salir
                      </a>
                    </div>
                  <?php } ?>
                </div>
                
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
            <!-- /Menu para el Usuario-->
          </div>
        </div>
        <!-- Menu de aplicaciones-->
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="<?php echo base_url();?>" class="nav-link active"><i class="fe fe-home"></i>Inicio</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-link"></i> Enlaces Internos</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a target="_blank" href="http://sistemas.sapi.gob.ve" class="dropdown-item ">SIPI</a>
                      <a target="_blank" href="http://sugerh.sapi.gob.ve/" class="dropdown-item ">SUGERH</a>
                      <a target="_blank" href="http://sigespenterprise.sapi.gob.ve/sigesp/inicio.html" class="dropdown-item ">SIGESP</a>
                      <a target="_blank" href="http://sire.sapi.gob.ve/" class="dropdown-item ">SIRE</a>
                      <a target="_blank" href="http://wiki.sapi.gob.ve/" class="dropdown-item">Ayudas y manuales</a>
                      <a target="_blank" href="http://correo.sapi.gob.ve/" class="dropdown-item ">Correo SAPI</a>
                      <a target="_blank" href="http://webpi.sapi.gob.ve/" class="dropdown-item ">WEBPI</a>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-file"></i> Paginas de Interes</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a target="_blank" href="http://mincomercionacional.gob.ve/" class="dropdown-item ">Ministerio del Poder Popular para el Comercio Nacional</a>
                      <a target="_blank" href="https://www.patria.org.ve" class="dropdown-item ">Patria</a>
                      <a target="_blank" href="https://enlinea.bt.gob.ve/DIBS_BDELTESORO/pages/s/login.jsp" class="dropdown-item ">Banco del Tesoro</a>
                      <a target="_blank" href="<?php echo base_url();?>theme/forgot-password.html" class="dropdown-item ">Petro App</a>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="http://www.sapi.gob.ve/" target="_blank" class="nav-link"><i class="fe fe-globe"></i>Pagina Principal SAPI</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>