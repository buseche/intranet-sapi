<footer class="footer">
  <div class="container">
    <div class="row align-items-center flex-row-reverse">
      <div class="col-auto ml-lg-auto">
        <div class="row align-items-center">
          <div class="col-auto">
            <ul class="list-inline list-inline-dots mb-0">
            </ul>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
        Copyleft 2020, Servicio Autónomo de la Propiedad Intelectual. Tema proporcionado por <a href="https://codecalm.net" target="_blank">codecalm.net</a> Todos los derechos reservados.
      </div>
    </div>
  </div>
</footer>
</div>
<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Salir del Sistema</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">¿Está seguro de terminar la sesion?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="<?php echo base_url();?>index.php/login/cerrarSesion">Salir</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>theme/vendor/jquery/jquery.min.js"></script>
  <!--PNotify-->
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotify.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>node_modules/pnotify/dist/iife/PNotifyButtons.js"></script>
  <!--Global ENV-->
  <script src="<?php echo base_url();?>theme/js/env_var.js"></script>
  <!--Recaptcha V3-->
  <script src="https://www.google.com/recaptcha/api.js?render=6LecrNUUAAAAAEwib6RrDklzMiUEiP3Rv5CSuYS9"></script>
  <script>
  grecaptcha.ready(function() {
    grecaptcha.execute('6LecrNUUAAAAAEwib6RrDklzMiUEiP3Rv5CSuYS9', {action: 'homepage'}).then(function(token) {
      document.getElementById('g-recaptcha-response').value=token;
    });
  });
  </script>
  <!--Script para el login-->
<script src="<?php echo base_url();?>theme/js/login.js"></script>
</body>
</html>