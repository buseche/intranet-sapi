<!-- Begin Page Content -->

<!--Seleccion de año-->
<div class="container-fluid">
    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Generacion de Constancias</h6>
        </div>
        <div class="card-body">
            <form id="constancias" method="POST">
            <div class="form-group row">
                    <label for="nomina" class="col-sm-4 col-form-label">Seleccione el tipo de constancia</label>
                </div>
                <div class="form-group row">
                    <div class="col-sm-7">
                        <input id="constM" class="form-control form-check-input" type="radio" name="tipoConstancia" value="1">
                        <label class="form-check-label cmensual" for="tipoConstancia">Constancia Mensual</label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-7">
                        <input id="constA" class="form-control form-check-input" type="radio" name="tipoConstancia" value="2">
                        <label id="canual" class="form-check-label " for="tipoConstancia">Constancia Anual</label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 align-self-center">
                        <button type="button" class="btn btn-primary" style="text-align: center" id="Confirmar">Consultar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!--Detalles del recibo de pago consultado-->
<div class="container-fluid" id="constancias-generadas">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detalles</h6>
        </div>
        <div class="card-body">
            <table class="table table-responsive table-borderless table-hover " id="detalles">
            </table>
        </div>
    </div>
</div>
<!-- Confirm Modal-->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Generar Constancias</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Al aceptar los cambios, se generarán las constancias seleccionadas y apareceran para su visualizacion y/o impresión.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <button class="btn btn-primary" type="button" id="generar">Aceptar</a>
        </div>
      </div>
    </div>
  </div>


</div>
<!-- End of Main Content -->