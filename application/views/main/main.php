        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pantalla principal</h1>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Fecha de hoy -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Hoy es</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <p class="fecha"></p>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Precio del petro del dia -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Precio del petro</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="ptrPrice"></div>
                    </div>
                    <div class="col-auto">
                      <img src="<?php echo base_url();?>theme/img/logo_petro2.svg">
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Hora del dia -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Hora</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><p id="clock"></p></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clock fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Tiempo del dia, cortesia de OpenWeather.com -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">El tiempo de hoy</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <p class="tiempo"></p>
                      </div>
                    </div>
                    <div class="col-auto icon">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4 d-none d-sm-none d-md-block">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Ultimas publicaciones</h6>
                </div>
                <div class="card-body">
                  <!--Noticias internas-->
                  <?php echo $noticias_int;?>
                </div>
              </div>

            </div>

            <div class="col-lg-6 mb-4 d-none d-sm-none d-md-block">

              <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Noticias Institucionales</h6>
                </div>
                <div class="card-body">
                <!--Noticias de la pagina-->
                  <?php echo $noticias_pag;?>
                </div>
              </div>
            </div>
          </div>
        <!-- /.container-fluid -->
        </div>
        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
      <!-- End of Main Content -->
