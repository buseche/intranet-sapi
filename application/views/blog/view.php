<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?php echo $titulo_post;?></h6>
        </div>
        <div class="card-body">
            <div class="col-12">
                <?php echo $content_post;?>
                <a class="btn btn-info" type="button" href="<?php echo base_url();?>main">Volver a la pantalla principal</a>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->