<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Crear Publicacion</h6>
        </div>
        <div class="card-body">
            <form method="POST" id="compose_post" class="form">
                <div class="form-group">
                    <label class="form-label" for="title">Titulo</label>
                </div>
                <div class="form-group">
                    <input name="title" class="form-control" id="titulo" value="<?php echo $titulo_post;?>">
                    <input name="id_post" type="hidden" id="id_post" value="<?php echo $id_post;?>">
                </div>
                <div class="form-group">
                    <label class="form-label" for="content">Contenido</label>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="content" id="contenido"><?php echo $content_post;?></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="button" id="publicar">Publicar</button>
                    <button class="btn btn-success" type="button" id="save_draft">Guardar borrador</button>
                    <a class="btn btn-danger" href="<?php echo base_url();?>main">Cancelar</a>
                </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->