<!-- Begin Page Content -->

<!--Seleccion de año-->
<div class="container-fluid">
    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Generacion de recibos</h6>
        </div>
        <div class="card-body">
            <form id="recibos">
                <div class="form-group row">
                    <label for="year" class="col-sm-4 col-form-label">Seleccione el año</label>
                    <div class="col-sm-7">
                        <select class="form-control" name="year" id="year">
                            <option>Seleccione año</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nomina" class="col-sm-4 col-form-label">Seleccione la nomina a Consultar</label>
                    <div class="col-sm-7">
                        <select class="form-control" name="nomina" id="nomina">
                            <option>Seleccione nomina</option>
                            <!--Consulta-->
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nomina" class="col-sm-4 col-form-label">Seleccione el rango a Consultar</label>
                    <div class="col-sm-7">
                        <select class="form-control" name="rango" id="rango">
                        <option>Seleccione rango</option>
                            <!--Consulta-->
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 align-self-center">
                        <button type="submit" class="btn btn-primary" style="text-align: center">Consultar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!--Detalles del recibo de pago consultado-->
<div class="container-fluid" id="seleccion-nomina">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detalles</h6>
        </div>
        <div class="card-body">
            <table class="table table-borderless table-hover table-responsive">
                <thead>
                    <tr>
                        <th scope="row">
                            Código
                        </th>
                        <th scope="row">
                            Descripcion
                        </th>
                        <th scope="row">
                            Asignaciones
                        </th>
                        <th scope="row">
                            Deducciones
                        </th>
                    </tr>
                </thead>
                <tbody id="detalles">
                    <!--Detalles de la tabla-->
            </table>
        </div>
    </div>
</div>


</div>
<!-- End of Main Content -->