<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Intranet SAPI">
  <meta name="author" content="Bryan Useche">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?php echo $titulo;?></title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- bootstrap-daterangepicker -->
  <link href="<?php echo base_url();?>theme/vendor/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
  <!-- bootstrap datatables-->
  <link href="<?php echo base_url();?>theme/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!--PNotify-->
  <link href="<?php echo base_url();?>node_modules/pnotify/dist/PNotifyBrightTheme.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();?>node_modules/material-design-icons/iconfont/material-icons.css" />
  <!--AnimateCSS-->
  <link href="<?php echo base_url();?>node_modules/animate.css/animate.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>theme/css/sb-admin-2.css" rel="stylesheet">
  
  
  

</head>