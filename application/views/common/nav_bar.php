<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav menu-sidebar-color sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url();?>index.php/main">
        <div class="sidebar-brand-icon">
          <img src="<?php echo base_url();?>theme/img/Logosapi-2020.png" style="max-width: 5rem; max-height: 3rem">
        </div>
        <div class="sidebar-brand-text mx-3">INTRANET</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url();?>index.php/main">
          <i class="fas fa-fw fa-home"></i>
          <span>Inicio</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Usuarios
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#apps" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-toolbox"></i>
          <span>Aplicaciones</span>
        </a>
        <div id="apps" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Consultas</h6>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/asistencia">Asistencia</a>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/recibos">Recibos de Pago</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Generacion de Constancias</h6>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/constancias">Constancias de Trabajo</a>
            <?php if($this->session->userdata('supervisor') == 't'){ ?>
            <h6 class="collapse-header">Publicaciones</h6>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/main/createPost">Crear Publicacion</a>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/main/managePost">Administrar Publicaciones</a>
            <?php }?>
            <?php if($this->session->userdata('cedula') == 'admin'){?>
            <h6 class="collapse-header">Administracion</h6>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/admin/users">Administrar Usuarios</a>
            <a class="collapse-item" href="<?php echo base_url();?>index.php/admin/blogs">Administrar Blogs</a>
            <?php }?>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Enlaces
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-location-arrow"></i>
          <span>Institucionales</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Internos</h6>
            <a class="collapse-item" target="_blank" href="http://sistemas.sapi.gob.ve/">SIPI</a>
            <a class="collapse-item" target="_blank" href="http://sigespenterprise.sapi.gob.ve/sigesp/inicio.html">SIGESP</a>
            <a class="collapse-item" target="_blank" href="http://sugerh.sapi.gob.ve/">SUGERH</a>
            <a class="collapse-item" target="_blank" href="http://sire.sapi.gob.ve/">SIRE</a>
            <a class="collapse-item" target="_blank" href="http://wiki.sapi.gob.ve/">Ayudas y Manuales</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Externos</h6>
            <a class="collapse-item" target="_blank" href="http://webpi.sapi.gob.ve">WEBPI</a>
            <a class="collapse-item" target="_blank" href="http://correo.sapi.gob.ve">Correo</a>
          </div>
        </div>
      </li>
      <!-- Nav Item - Paginas de interes -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#paginterest" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-globe-americas"></i>
          <span>Paginas de Interes</span>
        </a>
        <div id="paginterest" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Institucionales</h6>
            <a class="collapse-item" target="_blank" href="http://mincomercionacional.gob.ve/">Ministerio del Poder <br />Popular para el <br />Comercio Nacional</a>
            <a class="collapse-item" target="_blank" href="http://www.sapi.gob.ve">Página web SAPI</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Externos</h6>
            <a class="collapse-item" target="_blank" href="https://petroapp.petro.gob.ve/">Petro App</a>
            <a class="collapse-item" target="_blank" href="https://enlinea.bt.gob.ve/DIBS_BDELTESORO/pages/s/login.jsp">Banco del Tesoro</a>
            <a class="collapse-item" target="_blank" href="https://www.patria.org.ve">Patria</a>
          </div>
        </div>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!--Social Networks-->
            <li class="nav-item">
              <a class="nav-link p-2" target="_blank" href="https://www.facebook.com/SapiVenezuela/">
                <i class="fab fa-facebook"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link p-2" target="_blank" href="https://www.youtube.com/channel/UCOqdkLF9AcN7MGpHAtzW-1Q">
                <i class="fab fa-youtube"></i>	
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link p-2" target="_blank" href="https://www.instagram.com/sapi_venezuela/?hl=es-la">
                <i class="fab fa-instagram"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link p-2" target="_blank" href="https://twitter.com/sapi_ve">
                <i class="fab fa-twitter"></i>
              </a>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo ucwords(strtolower($this->session->userdata('nombre'))).' '.ucwords(strtolower($this->session->userdata('apellido')));?></span>
                <img class="img-profile rounded-circle" src="<?php echo base_url();?>theme/img/perfil.svg">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo base_url();?>index.php/perfil">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Salir
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->