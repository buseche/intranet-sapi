<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Reporte de asistencia</h6>
        </div>
        <div class="card-body">
            <p>Seleccione el rango de fechas para realizar la consulta</p>
            <form>
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-fw fa-calendar"></i></div>
                            </div>
                            <input type="text" class="form-control" id="fecha_inicio" placeholder="Fecha de inicio">
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-fw fa-calendar"></i></div>
                            </div>
                            <input type="text" class="form-control" id="fecha_fin" placeholder="Fecha Fin">
                        </div>
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary mb-2" id="enviar">Consultar</button>
                    </div>
                </div>
            </form>

            <br/>
            <div class="container">
                <div class="col-12">
                    <table class="table table-responsive" id="resultados">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" style="width:20%">Fecha</th>
                            <th scope="col" style="width:20%">Hora de Entrada</th>
                            <th scope="col" style="width:20%">Hora de Salida</th>
                            <th scope="col" style="width:20%">Tiempo Laborado</th>
                            <th scope="col" style="width:20%">Comentario</th>
                        </tr>
                    </thead>
                    <tbody class="consulta">
                        <!--Datos que se cargaran via JQuery-->
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->