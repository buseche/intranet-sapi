<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Constancia_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*Metodo para seleccionar el año y hacer las consultas en SIGESP*/
    public function selYear($year){
        switch($year){
            case 2017:
                $DB = $this->load->database('sigesp_2017', TRUE);
                return $DB;
            break;
            case 2018:
                $DB = $this->load->database('sigesp_2018', TRUE);
                return $DB;
            break;
            case 2019:
                $DB = $this->load->database('sigesp_2019', TRUE);
                return $DB;
            break;
            case 2020:
                $DB = $this->load->database('sigesp_2020', TRUE);
                return $DB;
            break;
        }
    }
    /*metodo para obtener las constancias generadas durante un periodo determinado*/
    public function listado_constancias($datos){
        $this->db->select('*');
        $this->db->from('sgd_constancias');
        $this->db->where('cedper', $datos['cedper']);
        $this->db->where('fechasol >= ', $datos['fecha_inicial']);
        $this->db->where('fechasol <= ', $datos['fecha_final']);
        $query = $this->db->get();
        return $query;
    }
    /*Metodo para contar cuantas constancias hay generadas en el periodo*/
    public function contar_constancias($datos, $tipconst){
        $this->db->select('COUNT(tipconst)');
        $this->db->from('sgd_constancias');
        $this->db->where('cedper', $datos['cedper']);
        $this->db->where('fechasol >= ', $datos['fecha_inicial']);
        $this->db->where('fechasol <= ', $datos['fecha_final']);
        $this->db->where('tipconst', $tipconst);
        $query = $this->db->get();
        $row = $query->row();
        $result = intval($row->count);
        return $result;
    }
    /*Metodo para obtener el sueldo y realizar los calculos para la constancia*/
    public function obtenerSueldo($codnom, $cedula){
        $DB = $this->selYear(date('Y'));
        $DB->select('a.codper, SUM(a.valsal)');
        $DB->from('sno_hsalida a, sno_hconcepto b, sno_personal c');
        if($codnom == '0004' || $codnom == '0005'){
            $DB->where("b.codconc = a.codconc AND a.codnom = b.codnom AND a.codper = c.codper AND a.codperi = b.codperi AND a.tipsal = 'A'");    
        }
        else{
            $DB->where("b.codconc = a.codconc AND a.codnom = b.codnom AND a.codper = c.codper AND a.codperi = b.codperi AND b.sueintcon = '1' AND a.tipsal = 'A'");
        }
        $DB->where('c.cedper', $cedula);
        $DB->where("a.valsal != '0' AND a.codnom !='0048'");
        $DB->where("a.codperi = (SELECT MAX(a.codperi) FROM sno_hsalida a WHERE a.codnom = '".$codnom."')");
        $DB->group_by('a.codper');
        $query = $DB->get();
        return $query;
    }
    /*Metodo para registrar la constancia en la base de datos*/
    public function registrarConstancia($datos){
        $this->db->insert('sgd_constancias', $datos);
        return TRUE;
    }

}
?>