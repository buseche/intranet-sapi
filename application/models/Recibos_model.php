<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recibos_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function selYear($year){
        switch($year){
            case 2017:
                $DB = $this->load->database('sigesp_2017', TRUE);
                return $DB;
            break;
            case 2018:
                $DB = $this->load->database('sigesp_2018', TRUE);
                return $DB;
            break;
            case 2019:
                $DB = $this->load->database('sigesp_2019', TRUE);
                return $DB;
            break;
            case 2020:
                $DB = $this->load->database('sigesp_2020', TRUE);
                return $DB;
            break;
        }
    }
    /*Metodo para seleccionar las nominas*/
    public function selNomina($year,$cedula){
        $BD_sigesp = $this->selYear($year);
        $BD_sigesp->distinct();
        $BD_sigesp->select('sno_hresumen.codnom, sno_nomina.desnom');
        $BD_sigesp->from('sno_hresumen');
        $BD_sigesp->join('sno_personal', 'sno_hresumen.codper = sno_personal.codper');
        $BD_sigesp->join('sno_nomina', 'sno_nomina.codnom = sno_hresumen.codnom');
        $BD_sigesp->where('sno_personal.cedper', $cedula);
        $BD_sigesp->where("NOT (sno_hresumen.codnom BETWEEN '0400' AND '0407')");
        $BD_sigesp->order_by('sno_hresumen.codnom', 'ASC');
        $consulta = $BD_sigesp->get();
        return $consulta;
    }
    /*Metodo para cargar los periodos*/
    public function cargarPeriodos($year, $cedula, $nomina){
        $DB = $this->selYear($year);
        $DB->select('sno_hresumen.codperi , sno_hperiodo.fecdesper, sno_hperiodo.fechasper');
        $DB->from('sno_hresumen');
        $DB->join('sno_hperiodo', 'sno_hresumen.codnom = sno_hperiodo.codnom AND sno_hresumen.codperi = sno_hperiodo.codperi');
        $DB->join('sno_personal', 'sno_hresumen.codper = sno_personal.codper');
        $DB->where('sno_hresumen.codnom', $nomina);
        $DB->where('sno_personal.cedper', $cedula);
        $DB->where("sno_hresumen.monnetres !='0'");
        $DB->order_by('sno_hresumen.codperi', 'DESC');
        $query = $DB->get();
        return $query;
    }
    /*Metodo para obtener los datos personales y la cuenta bancaria*/
    public function obtenercta($year, $cedula, $nomina){
        $DB = $this->selYear($year);
        $DB->select('sno_personal.codper, sno_personal.cedper, sno_personal.nomper, sno_personal.apeper, sno_personal.fecingper, sno_cargo.descar, sno_unidadadmin.desuniadm, sno_personalnomina.codcueban');
        $DB->from('sno_personal');
        $DB->join('sno_personalnomina', 'sno_personal.codper = sno_personalnomina.codper');
        $DB->join('sno_cargo', 'sno_cargo.codcar = sno_personalnomina.codcar AND sno_cargo.codnom = sno_personalnomina.codnom');
        $DB->join('sno_unidadadmin', 'sno_personalnomina.depuniadm = sno_unidadadmin.depuniadm AND sno_personalnomina.prouniadm = sno_unidadadmin.prouniadm');
        $DB->where('sno_personalnomina.codnom', $nomina);
        $DB->where('sno_personal.cedper', $cedula);
        $query = $DB->get();
        return $query;
    }
    /*Metodo para obtener los totales por periodo*/
    public function totales($year, $periodo, $cod_persona, $nomina){
        $DB = $this->selYear($year);
        $DB->select('sno_hresumen.codnom, sno_hresumen.codper, sno_hresumen.codperi, sno_hresumen.asires, sno_hresumen.dedres, sno_hresumen.apoempres, sno_hresumen.monnetres, sno_hperiodo.fecdesper, sno_hperiodo.fechasper');
        $DB->from('sno_hresumen');
        $DB->join('sno_hperiodo', 'sno_hresumen.codnom = sno_hperiodo.codnom');
        $DB->where('sno_hresumen.codnom', $nomina);
        $DB->where('sno_hresumen.codper', $cod_persona);
        $DB->where('sno_hresumen.codperi', $periodo);
        $query = $DB->get();
        return $query;
    }
    /*Metodo para obtener las asignaciones*/
    public function asignaciones($year, $cod_persona, $nomina, $periodo){
        $DB = $this->selYear($year);
        $DB->distinct();
        $DB->select('d.codnom, d.codper, d.codconc, e.nomcon, d.valsal, d.tipsal, d.codperi');
        $DB->from('sno_hsalida d, sno_hpersonalnomina a, sno_hconcepto e');
        $DB->where('e.codperi = d.codperi');
        $DB->where('e.codnom = d.codnom');
        $DB->where('e.codconc = d.codconc');
        $DB->where('a.codper = d.codper');
        $DB->where('d.codnom', $nomina);
        $DB->where('d.codper', $cod_persona);
        $DB->where('d.codperi', $periodo);
        $DB->where("d.valsal !='0'");
        $DB->where("d.tipsal ='A'");
        $DB->order_by('d.codconc', 'ASC');
        $query = $DB->get();
        return $query;
    }
    /*Metodo para las deducciones*/
    public function deducciones($year, $cod_persona, $nomina, $periodo){
        $DB = $this->selYear($year);
        $DB->distinct();
        $DB->select('d.codnom, d.codper, d.codconc, e.nomcon, d.valsal, d.tipsal, d.codperi');
        $DB->from('sno_hsalida d, sno_hpersonalnomina a, sno_hconcepto e');
        $DB->where('e.codperi = d.codperi');
        $DB->where('e.codnom = d.codnom');
        $DB->where('e.codconc = d.codconc');
        $DB->where('a.codper = d.codper');
        $DB->where('d.codnom', $nomina);
        $DB->where('d.codper', $cod_persona);
        $DB->where('d.codperi', $periodo);
        $DB->where("d.valsal !='0'");
        $DB->where("d.tipsal !='A'");
        $DB->where("d.tipsal !='P2'");
        $DB->where("d.tipsal !='R'");
        $DB->order_by('d.codconc', 'ASC');
        $query = $DB->get();
        return $query;
    }
    

}
?>