<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencia_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*Obtencion de reporte de asistencia*/
    public function obtener_reporte($datos){
        $DB2 = $this->load->database('asistencia', TRUE);
        $DB2->select('*');
        $DB2->from('asistencia');
        $DB2->where('asistencia.cedula', $datos['cedula']);
        $DB2->where('asistencia.fecha <=' , $datos['fecha_fin']);
        $DB2->where('asistencia.fecha >=' , $datos['fecha_inicio']);
        $DB2->order_by('fecha', 'ASC');
        $query = $DB2->get();
        return $query;
    }
    /*Obtencion de anexos*/
    public function obtener_anexos($cedula){
        $DB = $this->load->database('asistencia', TRUE);
        $DB->select('*');
        $DB->from('reganexos');
        $DB->where('cedula', $cedula);
        $query = $DB->get();
        return $query;
    }
    
}
?>