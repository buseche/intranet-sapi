<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*Metodo para obtener los post de la tabla de posts*/
    public function getPublish($autor){
        $this->db->select('*');
        $this->db->from('sgd_post');
        $this->db->where('author', $autor);
        $query = $this->db->get();
        return $query;
    }
    /*Metodo para obtener los borradores de la tabla de borradores*/
    public function getDraft($autor){
        $this->db->select('*');
        $this->db->from('sgd_draft');
        $this->db->where('author', $autor);
        $query = $this->db->get();
        return $query;
    }
    /*Metodo para chequear si existe un post*/
    public function check_id($id_post, $post_table){
        $this->db->select('*');
        $this->db->from($post_table);
        $this->db->where('id_post', $id_post);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /*Metodo para actualizar los posts*/
    public function updatePost($id_post, $title, $content, $autor, $date_post, $hour_post, $post_table){
        $this->db->set('title', $title);
        $this->db->set('content', $content);
        $this->db->set('author', $autor);
        $this->db->set('date_post', $date_post);
        $this->db->set('hour_post', $hour_post);
        $this->db->where('id_post', $id_post);
        $this->db->update($post_table);
        return TRUE;
    }
    /*Metodo para crear los posts*/
    public function createPost($id_post, $title, $content, $autor, $date_post, $hour_post, $post_table){
        $this->db->set('id_post', $id_post);
        $this->db->set('title', $title);
        $this->db->set('content', $content);
        $this->db->set('author', $autor);
        $this->db->set('date_post', $date_post);
        $this->db->set('hour_post', $hour_post);
        $this->db->insert($post_table);
        return TRUE;
    }
    /*Metodo para la edicion de un solo post en borradores*/
    public function getPost($id_post){
        $this->db->select('title, content');
        $this->db->from('sgd_draft');
        $this->db->where('id_post', $id_post);
        $query = $this->db->get();
        return $query;
    }
    /*Metodo para la eliminacion de borradores*/
    public function deleteDraft($id_post){
        $this->db->where('id_post', $id_post);
        $this->db->delete('sgd_draft');
        return TRUE;
    }
    /*Metodo que obtiene los posts directamente desde el wordpress de la pagina web*/
    public function getPostsWeb(){
        $post = array();
        $output = array();
        $DB = $this->load->database('pagina_web', TRUE);
        /*Esto obtiene el titulo y el enlace del post desde el primero de enero del año hasta el 31 de diciembre del año*/
        $DB->select('ID, post_title, guid');
        $DB->from('sp_posts');
        $DB->where('post_author', 5);
        $DB->where('post_status', 'publish');
        $DB->where('post_type', 'post');
        $DB->where("post_date BETWEEN '".date('Y')."-01-01 00:00:00' AND '".date('Y')."-12-31 00:00:00'");
        $DB->where("post_content NOT LIKE '%[gallery%'");
        $query = $DB->get();
        /*Generamos los valores de la base de datos y los guardamos en un array para procesarlos en el controlador*/
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $post['title'] = $row->post_title;
                $post['link']  = $row->guid;
                /*Obtenemos la imagen de cabecera*/
                if($this->getImagePost($row->ID)->post_mime_type == 'image/jpeg'){
                    $post['image'] = $this->getImagePost(intval($row->ID))->guid;
                }
                else if($this->getImagePost(intval($row->ID + 1) + 1)->post_mime_type == 'image/jpeg'){
                    $post['image'] = $this->getImagePost((intval($row->ID + 1) +1))->guid;
                }
                else{
                    $post['image'] = 'http://sapi.gob.ve/wp-content/uploads/2019/09/cintillo_home_sapi2020-scaled.jpg';
                }
                $output[] = $post;
            }
            return $output;
        }
        else{
            return 0;
        }
    }
    /*Metodo que obtiene las imagenes de la cabecera*/
    public function getImagePost($ID){
        $DB = $this->load->database('pagina_web', TRUE);
        $DB->select('*');
        $DB->from('sp_posts');
        $DB->where('ID', (intval($ID) + 1));
        $query = $DB->get();
        if(!is_null($query->row()->post_mime_type)){
            return $query->row();
        }
        else{
            return FALSE;
        }
    }
    /*Metodo para obtener los posts generados internamente*/
    public function getInternalPosts(){
        $this->db->select('*');
        $this->db->from('sgd_post');
        $this->db->order_by('date_post', 'DESC');
        $query = $this->db->get();
        return $query;
    }
}
?>