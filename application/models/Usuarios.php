<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*Metodo para seleccionar bd del año*/
    public function selYear(){
        $year = date('Y');
        switch($year){
            case '2017':
                $DB = $this->load->database('sigesp_2017', TRUE);
                return $DB;
            break;
            case '2018':
                $DB = $this->load->database('sigesp_2018', TRUE);
                return $DB;
            break;
            case '2019':
                $DB = $this->load->database('sigesp_2019', TRUE);
                return $DB;
            break;
            case '2020':
                $DB = $this->load->database('sigesp_2020', TRUE);
                return $DB;
            break;
        }
    }
    /*Metodo para verificar si el usuario ya esta registrado*/
    public function usuarioExiste($cedula){
        $this->db->select('cedper');
        $this->db->from('sgd_users');
        $this->db->where('cedper', $cedula);
        $query = $this->db->get();
        return $query;
    }
    /*Metodo para buscar el usuario en la bd de sigesp por defecto*/
    public function buscarUsuario($cedula){
        $DB = $this->selYear();
        $DB->select('*');
        $DB->from('sno_personal');
        $DB->where('cedper', $cedula);
        $query = $DB->get();
        return $query;
    }
    /*Metodo para autenticar el usuario*/
    public function autenticar($cedula, $clave){
        $this->db->select('*');
        $this->db->from('sgd_users');
        $this->db->where('cedper', $cedula);
        $this->db->where('passwd', $clave);
        $this->db->where('idstatus = 1');
        $query = $this->db->get();
        return $query;
    }
    /*Metodo para obtener la unidad administrativa del usuario*/
    public function buscarUA($cedula){
        $DB = $this->selYear();
        $DB->distinct();
        $DB->select("a.codnom, a.codper, b.depuniadm, b.prouniadm, c.desuniadm");
        $DB->from("sno_hresumen a, sno_personalnomina b, sno_unidadadmin c, sno_personal d");
        $DB->where("a.codnom = b.codnom");
        $DB->where("a.codper = b.codper AND a.codper = d.codper AND b.depuniadm = c.depuniadm AND b.prouniadm = c.prouniadm AND a.codnom IN ('0001', '0002', '0003', '0006', '0004', '0005')");
        $DB->where("d.cedper", $cedula);
        $DB->order_by("a.codnom", "DESC");
        $DB->limit(1);
        $query = $DB->get();
        $result = $query->row();
        return $result;
    }
    /*Registrar usuario*/
    public function registrarUsuario($datos){
        $this->db->insert('sgd_users', $datos);
        return TRUE;
    }
    /*Metodo que valida si la clave es la misma*/
    public function samePass($datos){
        $this->db->select('*');
        $this->db->from('sgd_users');
        $this->db->where('cedper', $datos['cedper']);
        $this->db->where('passwd', $datos['passwd']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /*Metodo que cambia la clave del usuario*/
    public function changePass($datos){
        $this->db->set('passwd', $datos['passwd']);
        $this->db->where('cedper' , $datos['cedper']);
        $this->db->update('sgd_users');
        return TRUE;
    }
    /*Metodos para el menu de administracion*/
    public function getAllUsers(){
        $this->db->select('*');
        $this->db->from('sgd_users');
        $query = $this->db->get();
        return $query;
    }
    public function getSingleUser($cedula){
        $this->db->select('*');
        $this->db->from('sgd_users');
        $this->db->where('cedper', $cedula);
        $query = $this->db->get();
        return $query;
    }

    public function isActiveUser($cedula){
        $this->db->select('*');
        $this->db->from('sgd_users');
        $this->db->where('cedper', $cedula);
        $this->db->where('idstatus', '1');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /*Metodo para obtener el Nombre y el correo de la persona*/

    public function getEmail($cedula){
        $DB = $this->selYear();
        $DB->select('b.cedper, b.nomper, b.apeper, b.coreleper');
        $DB->from('sno_personal b');
        $DB->where('b.cedper', $cedula);
        $query = $DB->get();
        return $query;
    }

    /*Metodo para seleccionar el codigo generado*/

    public function getCode($cedula){
        $this->db->select('*');
        $this->db->from('sgd_resetpass');
        $this->db->where('cedper', $cedula);
        $this->db->where('fecha_reset', date('Y-m-d'));
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $code = $query->row()->codval;
            return $code;
        }
        else{
            return NULL;
        }
    }
    /*Metodo para insertar el codigo generado*/
    public function insertCode($datos){
        $this->db->insert('sgd_resetpass',$datos);
        return TRUE;
    }
    /*Metodo para buscar el codigo generado*/
    public function selectCode($codval){
        $this->db->select('*');
        $this->db->from('sgd_resetpass');
        $this->db->where('codval', $codval);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    /*Metodo para eliminar el codigo de confirmacion y ademas cambiar la contraseña*/
    public function deleteCode($codval){
        $this->db->where('codval', $codval);
        $this->db->delete('sgd_resetpass');
        return TRUE;
    }
    /*Metodo para obtener los cumpleañeros del mes*/
    public function birthdays(){
        /*Seleccionamos la base de datos*/
        $DB = $this->selYear();
        /*Armamos el query*/
        $DB->distinct();
        $DB->select('d.cedper, d.nomper, d.apeper, extract(DAY FROM d.fecnacper) AS dia_nac, c.desuniadm');
        $DB->from('sno_hresumen a, sno_personalnomina b, sno_unidadadmin c, sno_personal d');
        $DB->where("a.codnom = b.codnom 
        AND a.codper = b.codper AND a.codper = d.codper AND b.depuniadm = c.depuniadm AND b.prouniadm = c.prouniadm AND a.codnom IN ('0001', '0002', '0003', '0006', '0004')
        AND d.fecegrper = '1900-01-01'");
        /*Aqui filtramos por fecha con la funcion TO_CHAR de postgres*/
        $DB->where("TO_CHAR(d.fecnacper, 'MM') = ", date('m'));
        $DB->order_by('dia_nac', 'ASC');
        $query = $DB->get();
        return $query;
    }
}
?>