<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Codeigniter_Curl{
    
    /*Metodo para la consulta de recaptchaV3*/

    public function curl_get(){
        

    }
    /*Metodo para POST*/

    public function curl_post($url, $params){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $error    = curl_error($ch);
        $errno    = curl_errno($ch);

        if (is_resource($ch)) {
            curl_close($ch);
        }

        if (0 !== $errno) {
            throw new \RuntimeException($error, $errno);
        }

        return $response;
    }
    

}