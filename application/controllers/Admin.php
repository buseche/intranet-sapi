<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct(){
		parent::__construct();
        $this->load->model('Usuarios');
        $this->load->model('Blog_model');
    }
    /*Metodo para cargar la vista de administracion de usuarios*/
    public function users(){
        $data = $this->fetch_all_data_user();
        $this->load->view('common/header', $data);
        $this->load->view('common/nav_bar');
        $this->load->view('admin/users', $data);
    }
    /*Metodo para obtener los datos de todos los usuarios de la intranet*/
    public function fetch_all_data_user(){
        $data = array();
        $data['titulo'] = "Intranet SAPI - Administracion de Usuarios";
        $table = '
            <thead>
                <tr>
                    <td>Cedula</td>
                    <td>Nombres</td>
                    <td>Apellidos</td>
                    <td>Departamento Adscrito</td>
                    <td>Status</td>
                    <td>Acciones</td>
                </tr>
            </thead>
            <tbody>
        ';
        $query = $this->Usuarios->getAllUsers();
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $table .= '
                <tr>
                    <td width="10%">'.$row->cedper.'</td>
                    <td width="20%">'.$row->nomper.'</td>
                    <td width="20%">'.$row->apeper.'</td>
                    <td width="35%">'.$row->desuniadm.'</td>
                    ';
                    if($row->idstatus == "1"){
                        $table .= '
                        <td width="10%">Activo</td>
                        ';
                    }
                    else{
                        $table .= '<td width="10%">Inactivo</td>';
                    }
                    $table .= '
                    <td width="5%"><button class="btn btn-info editar" id="'.trim($row->cedper).'"><i class="fas fa-edit"></i></button><button class="btn btn-danger inhabilitar" id="'.trim($row->cedper).'"><i class="fas fa-trash"></i></button></td>
                </tr>
                ';
            }
        }
        else{
            $table .= 
            '<tr>
                <td colspan="6" style="text-align:center;">No se encontraron registros</td>
            </tr>';
        }
        $data['table'] = $table;
        return $data;
    }
    /*Metodo para obtener la informacion acerca de si es supervisor, y la foto de perfil del usuario*/
    public function getInfoUser(){
        $response = array();
        $datos = trim($this->input->post('cedula'));
        $query = $this->Usuarios->getSingleUser($datos);
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $response['cedula']     = $row->cedper;
                $response['supervisor'] = $row->supervisor;
                $response['estatus']    = $row->idstatus;
                $response['avatar']     = $row->photo;
            }
        }
        else{
            $response['status']  = 404;
            $response['message'] = 'Not Found';
        }
        echo json_encode($response);
    }

    /*Metodo para actualizar el usuario*/
    public function updateUser(){
        //Arreglo con los datos para editar el usuario
        $datos = array();
        $datos['supervisor'] = $this->input->post('supervisor');
        $datos['estatus'] = $this->input->post('estatus');
        $datos['cedula'] = $this->input->post('cedula');
        //Arreglo con la respuesta
        if(isset($_FILES["avatar"]["name"])){
            //Seteamos el directorio de almacenamiento
            $config['upload_path'] = './theme/img/avatars';
            //Le decimos que solo aceptaremos imagenes 
            $config['allowed_types'] = 'jpg|jpeg|png|gif';  
            //Esto indica que si el archivo existe lo sobreescriba
            $config['overwrite'] = TRUE;
            //Indicamos el tamaño maximo del archivo en KB
            $config['max_size'] = 512;
            //Inicializamos la libreria;
            $this->load->library('upload', $config);
            $isUpload = $this->upload->do_upload('avatar');
            if($isUpload){  
                $data = $this->upload->data();  
                $datos['photo'] = $config['upload_path'].'/'.$data['file_name'];
            }
        }
        $datos['pass'] = $this->input->post('pass');
        var_dump($datos);
    }
}
?>