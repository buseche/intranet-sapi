<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Constancia extends CI_Controller {
    public function __construct(){
      parent::__construct();
      $this->load->model('Constancia_model');
      $this->load->model('Usuarios');
      $this->load->model('Recibos_model');
    }
    /*Variables del periodo para ser vistas de manera global*/
    public $dia_inicio = '01';
    public $dia_inicio_2 = '16';
    public $dia_fin = '15';
    public $dia_fin_2 = '';
    
    /*Metodo para generar codigos aleatorios*/
    public function generaCodigo($longitud){
      $codigo = "";
      $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      $max = strlen($caracteres)-1;
      for ($i = 0; $i < $longitud; $i++) {
        $codigo .= $caracteres[rand(0, $max)];
      }
      return $codigo;
    }
    /*Metodo para invertir las fechas*/
    public function invertirFecha($fecha){
      $date1 = explode('-', $fecha);
      $date2 = $date1[2]."/".$date1[1]."/".$date1[0];
      return $date2;
    }
    
    /*Metodo para obtener las constancias generadas por usuario dentro de un periodo determinado*/
    public function getConstancia(){
      /*Seteamos los valores del mes y el año*/
      $year = date('Y');
      $mes = date('m');
      /*Datos a pasar al modelo*/
      $datos = array();
      /*Tabla a imprimir con las constancias*/
      $table = '
      <thead>
      <tr>
      <th scope="row" width="16%">Fecha</th>
      <th scope="row" width="16%">Cargo</th>
      <th scope="row" width="16%">Unidad Administrativa</th>
      <th scope="row" width="16%">Sueldo Bs.</th>
      <th scope="row" width="16%">Tipo</th>
      <th scope="row" width="16%">Acciones</th>
      </tr>
      </thead>
      <tbody>
      <!--Detalles de la tabla-->
      </tbody>';
      /*Array de respuesta*/
      $response = array();
      /*Verificamos que el mes termine en 30, 31 , 28 ó 29 */
      /*Validando que termine en los meses que terminan en 31*/
      if(date('m') == '01' || date('m') == '03' || date('m') == '05' || date('m') == '07' || date('m') == '08' || date('m') == '10' || date('m') == '12'){
        $this->dia_fin_2 = '31';
      }
      /*Validando que si el mes es febrero*/
      else if(date('m') == '02'){
        $this->dia_fin_2 = '28';
      }
      /*El resto de los meses tendra 30*/
      else{
        $this->dia_fin_2= '30';
      }
      /*Seteamos los valores para hacer la busqueda*/
      $datos['cedper']        = $this->session->userdata('cedula');
      $datos['fecha_inicial'] = $year.'-'.$mes.'-'.$this->dia_inicio;
      $datos['fecha_final']   = $year.'-'.$mes.'-'.$this->dia_fin_2;
      /*Preguntamos al modelo por las constancias generadas en ese periodo*/
      $query = $this->Constancia_model->listado_constancias($datos);
      /*Arreglo que pasaremos encriptado para obtener los datos en el contenedor*/
      $request = array();
      /*Pasamos los datos que necesita*/
      $request['cedula'] = $this->session->userdata('cedula');
      $request['codper'] = $this->session->userdata('id_user');
      $request['codnom'] = $this->Usuarios->buscarUA($this->session->userdata('cedula'))->codnom;
      /*Verificamos que no haya llegado vacio*/
      if($query->num_rows() > 0){
        foreach($query->result() as $row){
          $table .= '
          <tr>
          <th scope="row">'.$this->invertirFecha($row->fechasol).'</th>
          <td>'.$row->denasicar.'</td>
          <td>'.$row->desuniadm.'</td>
          <td>'.$row->valsal.'</td>';
          if($row->tipconst == '1'){
            $table .= '
            <td>Mensual</td>
            <td><a target="_blank" href="http://constancias.sapi.gob.ve/action/ver_m_constancia.php?codigo='.$row->codval.'&fecha='.$this->invertirFecha($row->fechasol).'&nomina='.$request['codnom'].'&user_ced='.$request['cedula'].'&user_id='.$request['codper'].'"><i class="fas fa-file-pdf"></i> Ver Constancia</a></td>
            ';
          }
          else{
            $table .= '
            <td>Anual</td>
            <td><a target="_blank" href="http://constancias.sapi.gob.ve/action/ver_a_constancia.php?codigo='.$row->codval.'&fecha='.$this->invertirFecha($row->fechasol).'&nomina='.$request['codnom'].'&user_ced='.$request['cedula'].'&user_id='.$request['codper'].'"><i class="fas fa-file-pdf"></i> Ver Constancia</a></td>
            ';
          }
          $table .= '</tr>';
        }
      }
      else{
        $table .= '<tr><td colspan="6" style="text-align: center">No tiene constancias generadas</td></tr>';
      }
      /*Obtenemos el numero de constancias mensual y anual*/
      $nro_const_m = $this->Constancia_model->contar_constancias($datos, '1');
      $nro_const_a = $this->Constancia_model->contar_constancias($datos, '2');
      /*Armamos arreglo de respuesta*/
      $response['status']         = 200;
      $response['message']        = "Consulta generada exitosamente";
      $response['table']          = $table;
      $response['const_mensuales']= $nro_const_m;
      $response['const_anuales']  = $nro_const_a;
      echo json_encode($response);
    }
    /*Metodo para generar las constancias*/
    public function generarConstancia(){
      /*Seteamos los valores del mes y el año*/
      $year = date('Y');
      $mes = date('m');
      /*Array de datos con los datos para generar la constancia*/
      $datos = array();
      /*Recepcion de los datos para generar la constancia*/
      $opt = $this->input->post('tipo_const');;
      /*Obtencion del codigo de nomina de la persona*/
      $datos_nom = $this->Usuarios->buscarUA($this->session->userdata('cedula'));
      /*Obtener datos de cuenta bancaria*/
      $datos_bancarios = $this->Recibos_model->obtenercta($year, $this->session->userdata('cedula'),$datos_nom->codnom);
      /*Obtencion del sueldo para generar la constancia*/
      $query = $this->Constancia_model->obtenerSueldo($datos_nom->codnom, $this->session->userdata('cedula'));
      /*Dependiendo de que recibamos via post, generamos los calculos para la constancia*/
      switch($opt){
        /*Caso 1: Constancia Mensual*/
          case 1:
            if($query->num_rows() > 0){
              foreach($query->result_array() as $row){
                /*Calculamos el valor del sueldo y lo guardamos en el array*/
                $datos['valsal'] = number_format(($row['sum'] * 2), 2, ",",".");
              }
              /*Seteamos los datos restantes*/
              $datos['codper'] = $datos_nom->codper;
              $datos['cedper'] = $this->session->userdata('cedula');
              $datos['nomper'] = $this->session->userdata('nombre');
              $datos['apeper'] = $this->session->userdata('apellido');
              $datos['codval'] = $this->generaCodigo(10);
              $datos['tipconst'] = 1;
              $datos['denasicar'] = $datos_bancarios->row()->descar;
              $datos['desuniadm'] = $datos_bancarios->row()->desuniadm;
              $datos['fechasol'] = $year.'-'.$mes.'-'.date('d');
              /*Insertamos en la base de datos*/
              $result = $this->Constancia_model->registrarConstancia($datos);
              if($result){
                $response['status'] = 200;
                $response['message'] = "Constancia generada exitosamente";
              }
              else{
                $response['status'] = 500;
                $response['message'] = "Error al insertar en base de datos";
              }
              echo json_encode($response);
            }
            else{
              $response['status'] = 404;
              $response['message'] = "Aún no se han generado nóminas, por favor intentelo más tarde";
              echo json_encode($response);
            }
          break;
          case 2:
            if($query->num_rows() > 0){
              $salario_base = 0;
              /*Obtenemos el salario del usuario*/
              foreach($query->result_array() as $row){
                $salario_base = $row['sum'] * 2;
              }
              $sal_mensual    = number_format($salario_base, 2, ",", ".");
              $el_sal_anual   = $salario_base*12;
              $sal_anual      = number_format($el_sal_anual, 2, ",", ".");
              $sal_diario     = $salario_base/30;
              $cuota_vac      = ($sal_diario*50)/360;
              $sueldo_int     = (($sal_diario*50)/360)+((($sal_diario+$cuota_vac)*120)/360)+$sal_diario;
              $la_vacaciones  = $sueldo_int*50;
              $vacaciones     = number_format($la_vacaciones, 2, ",", ".");
              $la_utilidades  = $sueldo_int*120;
              $utilidades     = number_format($la_utilidades, 2, ",", ".");
              $el_total_anual = $el_sal_anual+$la_utilidades+$la_vacaciones;
              $el_total_anual = number_format($el_total_anual, 2, ",", ".");
              /*Seteamos los campos para insertar en la base de datos*/
              $datos['codper']    = $this->session->userdata('id_user');
              $datos['cedper']    = $this->session->userdata('cedula');
              $datos['nomper']    = $this->session->userdata('nombre');
              $datos['apeper']    = $this->session->userdata('apellido');
              $datos['codval']    = $this->generaCodigo(10);
              $datos['tipconst']  = 2;
              $datos['denasicar'] = $datos_bancarios->row()->descar;
              $datos['desuniadm'] = $datos_bancarios->row()->desuniadm;
              $datos['fechasol']  = $year.'-'.$mes.'-'.date('d');
              $datos['valsal']    = $el_total_anual;
              /*Insertamos en la base de datos*/
              $result = $this->Constancia_model->registrarConstancia($datos);
              /*Si el resultado es correcto, informamos al usuario*/
              if($result){
                $response['status'] = 200;
                $response['message'] = "Constancia generada exitosamente";
              }
              else{
                $response['status'] = 500;
                $response['message'] = "Error al insertar en base de datos";
              }
              echo json_encode($response);
            }
            else{
              $response['status'] = 404;
              $response['message'] = "Aún no se han generado nóminas, por favor intentelo más tarde";
              echo json_encode($response);
            }
          break;
        }
    }

}
?>