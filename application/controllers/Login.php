<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Usuarios');
        $this->load->library('Codeigniter_Curl');
        $this->load->model('Blog_model');
    }
    /*Vista por defecto de la vista*/
	public function index(){
        if($this->session->userdata('logged')){
            redirect(base_url().'index.php/main');
        }
        else{
            $reg_vlan = "#^(192\.8\.18\.(25[012345]|2[01234]\d|[01]?\d\d?))$#";
            $data['titulo'] = 'Intranet SAPI - Pagina Principal';
            $data['noticias_pag'] = $this->getPostsWeb();
            $data['noticias_int'] = $this->getInternalPosts();
            $data['birthdays']    = $this->getBirthdays();
            /*Si el usuario es interno, le muestra la pagina de inicio con los posts*/
            if(preg_match("#^(201\.249\.201\.206)$#", $_SERVER['REMOTE_ADDR']) == 1 || preg_match($reg_vlan, $_SERVER['REMOTE_ADDR']) == 1){
                $this->load->view('homepage/header.php', $data);
                $this->load->view('homepage/nav_bar.php');
                $this->load->view('homepage/content.php', $data);
                $this->load->view('homepage/footer.php');
            }
            /*Si no, el inicio normal */
            else{
                $data['titulo'] = "Intranet SAPI";
                $this->load->view('common/header.php', $data);
                $this->load->view('login/login.php');
            }
        }
        
    }
    /*Metodo para cargar la vista de registro con los datos del usuario*/
    public function registrar(){
        $datos_usuario = json_decode(base64_decode($this->input->get('q')));
        $datos['titulo'] = 'Intranet SAPI - Registro del usuario';
        $this->load->view('common/header',$datos);
        $this->load->view('login/registro', $datos_usuario);
    }
    /*Metodo para autenticacion del usuario*/
    public function entrar(){
        /*Arreglo para la respuesta*/
        $response = array();
        /*Obtencion de los datos*/
        $datos = json_decode(base64_decode($this->input->post('datos')));
        /*Arreglo para el registro*/
        $data_registro = array();
        /*arreglo para los datos de la sesion*/
        $datos_sesion = array();
        /*Verificamos si el usuario es un robot o no*/
        $reCaptcha = array('secret' => '6LecrNUUAAAAABzcMyre12vozLVbBxiOek_jiIVr', 'response' => $datos->reCaptcha_response);
        $isRobot = json_decode($this->codeigniter_curl->curl_post('https://www.google.com/recaptcha/api/siteverify', $reCaptcha));
        /*Si es exitoso y el score es mayor a 0.5*/
        if($isRobot->success && $isRobot->score > 0.5){
            $response['score'] = $isRobot->score;
            /*Verficamos si la cedula y la clave son las mismas, para chequear el registro*/
            if($datos->cedula == $datos->clave){
                /*Si son iguales, verificamos que no este registrado en el sistema*/
                $query = $this->Usuarios->usuarioExiste(trim($datos->cedula));
                /*Si no esta registrado, lo buscamos en SIGESP*/
                if($query->num_rows() < 1){
                    /*Buscamos el usuario en SIGESP*/
                    $datos_usuario = $this->Usuarios->buscarUsuario(trim($datos->cedula));
                    /*Si no esta alla*/
                    if($datos_usuario->num_rows() < 1){
                        $response['status'] = 404;
                        $response['message'] = "Usuario no registrado en el Sapi";
                    }
                    /*Si esta, armar el array con los datos y proceder al registro*/
                    else{
                        foreach($datos_usuario->result() as $row){
                            $data_registro['cedula']      = $row->cedper;
                            $data_registro['id_emp']      = $row->codper;
                            $data_registro['nombre']      = $row->nomper;
                            $data_registro['apellido']    = $row->apeper;
                            $data_registro['email']       = $row->coreleper;
                            $data_registro['nacionalidad']= $row->nacper;
                            $data_registro['fecha_ing']   = $row->fecingper;
                            $response['status'] = 201;
                            $response['datos'] = base64_encode(json_encode($data_registro));
                        }
                    }
                }
                /*Si lo esta, lo indicamos*/
                else{
                    $response['status']  = 301;
                    $response['message'] = 'Usuario ya registrado en el sistema';
                }
            }
            else{
                $cedula = $datos->cedula;
                $clave = md5($datos->clave);
                $query = $this->Usuarios->autenticar($cedula , $clave);
                if($query->num_rows() > 0){
                    foreach($query->result() as $row){
                        /*Obtenemos los datos de la sesion*/
                        $datos_sesion['id_rol']  = $row->idrol;
                        $datos_sesion['cedula']  = trim($row->cedper);
                        $datos_sesion['id_user'] = $row->codper;
                        $datos_sesion['nombre']  = $row->nomper;
                        $datos_sesion['apellido']= $row->apeper;
                        $datos_sesion['logged']  = TRUE;
                        $datos_sesion['supervisor'] = $row->supervisor;
                        $datos_sesion['avatar'] = $row->photo;
                        $this->session->set_userdata($datos_sesion);
                        $response['status'] = 200;
                        $response['message'] = "Usuario logeado";
                    }
                }
                else{
                    $response['status'] = 404;
                    $response['message'] = "Usuario o contraseña incorrectos";
                }
            }
        }
        else{
            $response['status'] = 403;
            $response['message'] = "You're a robot, we don't want robots here, go away";
        }
        /*Salida para el usuario*/
        echo json_encode($response);
    }
    /*Metodo para terminar de registrar al usuario*/
    public function signin(){
        /*Arreglo con los datos para iniciar de una vez la sesion*/
        $datos_sesion = array();
        /*Arreglo de respuesta*/
        $response = array();
        /*Datos basicos del usuario del formulario*/
        $datos_basicos = $this->input->post('datos');
        /*Datos del usuario encriptados*/
        $datos_usuario = json_decode(base64_decode($datos_basicos['data']));
        /*Buscamos los datos administrativos del usuario*/
        $datos_departamento = $this->Usuarios->buscarUA($datos_usuario->cedula);
        /*Datos a pasar al modelo*/
        $data = array();
        $data['cedper']  = $datos_usuario->cedula;
        $data['codper']  = $datos_usuario->id_emp;
        $data['nomper']  = $datos_usuario->nombre;
        $data['apeper']  = $datos_usuario->apellido;
        $data['passwd']  = md5($datos_basicos['clave']);
        /*Definimos que tipo de departamento*/
        if($datos_departamento->codnom == 0006){
            $data['desuniadm'] = 'Sin asignacion de cargo';
            $data['idrol']   = 5;
        }
        else{
            $data['desuniadm'] = $datos_departamento->desuniadm;
            $data['idrol']   = 3;
        }
        $data['idstatus']= 1;
        
        /*Registramos al usuario*/
        $query = $this->Usuarios->registrarUsuario($data);
        /*Si es correcto, seteamos su sesion para que pueda usar de una vez su intranet*/
        if($query){
            $datos_sesion['id_rol']  = $data['idrol'];
            $datos_sesion['cedula']  = trim($data['cedper']);
            $datos_sesion['id_user'] = $data['codper'];
            $datos_sesion['nombre']  = $data['nomper'];
            $datos_sesion['apellido']= $data['apeper'];
            $datos_sesion['logged']  = TRUE;
            $datos_sesion['supervisor'] = FALSE;
            $this->session->set_userdata($datos_sesion);
            $response['status'] = 200;
            $response['message'] = "Usuario logeado";
        }
        else{
            $response['status']  = 500;
            $response['message'] = "Hubo un problema de conexion con la base de datos";
        }
        echo json_encode($response);
    }
    /*Metodo para cargar la vista de recuperacion de clave*/
    public function recuperar(){
        $data['titulo'] = 'Intranet - Recuperar clave';
        $this->load->view('common/header.php', $data);
        $this->load->view('login/restore.php');
    }
    /*Metodo para cerrar la sesion*/
    public function cerrarSesion(){
        $this->session->sess_destroy();
		redirect(base_url().'index.php/login');
    }
    /*Metodo para generar codigos aleatorios*/
    public function generaCodigo($longitud){
        $codigo = "";
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $max = strlen($caracteres)-1;
        for ($i = 0; $i < $longitud; $i++) {
          $codigo .= $caracteres[rand(0, $max)];
        }
        return $codigo;
    }
    /*Metodo para enviar el codigo al usuario*/
    public function enviarCodigo(){
        /*Dato que pasaremos via POST*/
        $user  = trim($this->input->post('cedula'));
        $fecha = date("Y-m-d");
        $datos_correo = array();
        $datos_correo['cedper'] = $user;
        $opt = TRUE;
        /*Response para trabajar con Javascript*/
        $response = array();
        /*Validamos que el usuario este activo*/
        $q = $this->Usuarios->isActiveUser($user);
        /*Si esta activo buscamos el correo*/
        if($q){
            $query = $this->Usuarios->getEmail($user);
            if($query->num_rows() > 0){
                foreach($query->result() as $row){
                    $datos_correo['coreleper'] = $row->coreleper;
                    $nombre = $row->nomper.' '.$row->apeper;
                }
            }
            else{
                $response['status'] = 404;
                $response['message'] = "Usuario no posee registro en el sistema SIGESP, por favor, dirijase a la Coordinacion de Gestion Humana para la actualizacion de sus datos";    
            }
            /*Verificamos si no ha generado anteriormente un codigo en el dia, asi le podremos enviar nuevamente el codigo*/
            $query = $this->Usuarios->getCode($user);
            if(is_null($query)){
                $datos_correo['codval'] = $this->generaCodigo(6);
                $datos_correo['fecha_reset'] = date('Y-m-d');
                $ins = $this->Usuarios->insertCode($datos_correo);
            }
            else{
                $datos_correo['codval'] = $query;
            }
            $response['status'] = 200;
            $response['message'] = "Codigo generado exitosamente";
        }
        else{
            $response['status'] = 404;
            $response['message'] = "Usuario no registrado o no activo en el SAPI";
        }
        $response['datos_correo'] = $datos_correo;
        echo json_encode($response);
    }
    /*Metodo para confirmar el codigo*/
    public function confirmarCodigo(){
        $response = array();
        $codval = trim($this->input->post('codval'));
        $query = $this->Usuarios->selectCode($codval);
        if($query){
            $del = $this->Usuarios->deleteCode($codval);
            if($del){
                $response['status'] = 200;
                $response['message'] = 'Codigo confirmado';
            }
            else{
                $response['status'] = 500;
                $response['message'] = "Hubo un error en la base de datos";
            }
        }
        else{
            $response['status'] = 404;
            $response['message'] = 'Codigo no encontrado';
        }
        echo json_encode($response);
    }
    /*Metodo para realizar el cambio de contraseña del usuario*/
    public function changePass(){
        /*Resto de acciones*/
        $response = array();
        $datos = json_decode(base64_decode($this->input->post('datos')));
        $data = array();
        $data['cedper'] = $datos->user;
        $data['passwd'] = md5($datos->pass);
        $chkpass = $this->Usuarios->samePass($data);
        /*Validamos que la clave no sea igual a la cedula*/
        if($data['cedper'] == $datos->pass){
            $response['status']  = 403;
            $response['message'] = 'La clave no puede ser igual a la cedula de identidad';
        }
        /*Validamos que no sea 12345*/
        else if($data['passwd'] == md5('12345') || $data['passwd'] == md5('123456')){
            $response['status']  = 403;
            $response['message'] = 'La clave no puede ser del 1 al 5';
        }
        /*Validamos si la clave no es la anterior*/
        else if($chkpass){
            $response['status']  = 403;
            $response['message'] = 'La clave no puede ser igual a la anterior';
        }
        else{
            $update = $this->Usuarios->changePass($data);
            if($update){
                $response['status']  = 200;
                $response['message'] = 'Contraseña cambiada exitosamente';
            }
        }
        echo json_encode($response);
    }
    /*Metodo para obtener los posts de la pagina web y mostrarlos*/
    public function getPostsWeb(){
        $response = array();
        $query = $this->Blog_model->getPostsWeb();
        $salida = '
        <div id="noticias_sapi" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">';
        $count = 0;
        foreach($query as $row){
            if($count == 0){
                $salida .= '
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="'.$row['image'].'" alt="'.$row['title'].'" style="height=576px; width=384px;">
                        <div class="carousel-caption d-none d-md-block titulo_noticia2">
                            <h5><a target="_blank" href="'.$row['link'].'">'.$row['title'].'</a></h5>
                        </div>
                    </div>';
            $count++;
            }
            else{
                $salida .= '
                    <div class="carousel-item">
                        <img class="d-block w-100" src="'.$row['image'].'" alt="'.$row['title'].'" style="width: 576px; height: 384px;">
                        <div class="carousel-caption d-none d-md-block titulo_noticia2">
                            <h5><a target="_blank" href="'.$row['link'].'">'.$row['title'].'</a></h5>
                        </div>
                    </div>';
            }
        }
        $salida .= '
            </div>
            <a class="carousel-control-prev" href="#noticias_sapi" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#noticias_sapi" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>';
        return $salida;
    }
    /*Metodo para obtener los posts internos*/
    public function getInternalPosts(){
        $query = $this->Blog_model->getInternalPosts();
        $salida = '
        <div id="noticias_int">
        <ul>';
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                    $salida .= '<li><h5><a href="'.base_url().'blog/viewPost?id_post='.$row->id_post.'">'.$row->title.'</a></h5></li>';
            }
        }
        else{
            if($this->session->userdata('id_rol') == '4'){
                $salida .= '
                    <li><h5>Aún no se han publicado entradas, <a href="'.base_url().'main/createPost">¿Porqué no publicas una?</a></h5></li>';
            }
            else{
                $salida .= '
                    <li><h5>Aún no se han publicado entradas, disfruta del dia</h5></li>';
            }
        }
        $salida .= '
        </ul>
        </div>';
        return $salida;
    }
    /*Metodo para obtener los cumpleañeros y pasarlos a la vista interna*/
    public function getBirthdays(){
        $table = '<table class="table table-hover table-outline table-vcenter text-nowrap card-table">
        <thead>
          <tr>
            <!--Foto-->
            <th class="text-center w-1"><i class="icon-people"></i></th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th class="text-center">Dia de Cumpleaños</th>
            <th>Departamento</th>
          </tr>
        </thead>
        <tbody>';
        $query = $this->Usuarios->birthdays();
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $table .= '
                <tr>
                <td class="text-center">';
                $getPhoto = $this->getProfilePhoto($row->cedper);
                if($getPhoto->num_rows() > 1){
                    foreach($getPhoto->result() as $index){
                        $table .= '
                        <div class="avatar d-block" style="background-image: url('.$index->photo.')">
                        </div>';        
                    }
                }
                else{
                    $table .= '
                    <div class="avatar d-block" style="background-image: url('.base_url().'theme/img/perfil.svg">
                    </div>';
                }
                $table .= '
                </td>
                <td>
                    <div>'.mb_convert_case(trim($row->nomper), MB_CASE_TITLE, "UTF-8").'</div>
                </td>
                <td>
                <div>'.mb_convert_case(trim($row->apeper), MB_CASE_TITLE, "UTF-8").'</div>
                </td>
                <td>
                    <div class="text-center">'.$row->dia_nac.'</div>
                </td>
                <td>
                    <div>'.mb_convert_case(trim($row->desuniadm), MB_CASE_TITLE, "UTF-8").'</div>
                </td>
                </tr>';
            }
        }
        else{
            $table .= '<tr><td><div colspan="5">No hay cumpleañeros este mes</div>';
        }
        $table .= '</tbody></table>';
        return $table;
    }
    /*Metodo para obtener las fotos internas de los trabajadores*/
    public function getProfilePhoto($cedula){
        $this->db->select('photo');
        $this->db->from('sgd_users');
        $this->db->where('cedper', $cedula);
        $query = $this->db->get();
        return $query;
    }
}