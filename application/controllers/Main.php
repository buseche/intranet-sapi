<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Usuarios');
        $this->load->model('Blog_model');
        $this->load->library('Codeigniter_Curl');
      }
    
    /*Metodo de carga por defecto*/
    public function index(){
        if($this->session->userdata('logged')){
            $data['titulo'] = 'Intranet SAPI - Pagina Principal';
            $data['noticias_pag'] = $this->getPostsWeb();
            $data['noticias_int'] = $this->getInternalPosts();
            $this->load->view('common/header.php', $data);
            $this->load->view('common/nav_bar.php');
            $this->load->view('main/main.php', $data);
            $this->load->view('main/footer.php');
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    /*Metodo para cargar la vista del reporte de asistencia*/
    public function asistencia(){
        if($this->session->userdata('logged')){
            $data['titulo'] = 'Intranet SAPI - Reporte de Asistencia';
            $this->load->view('common/header.php', $data);
            $this->load->view('common/nav_bar.php');
            $this->load->view('asistencia/main.php');
            $this->load->view('asistencia/footer.php');
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    /*Metodo para cargar la vista del reporte para la generacion de recibos de pago*/
    public function recibos(){
        if($this->session->userdata('logged')){
            $data['titulo'] = 'Intranet SAPI - Generacion de recibos de pago';
            $this->load->view('common/header.php', $data);
            $this->load->view('common/nav_bar.php');
            $this->load->view('recibos/main.php');
            $this->load->view('recibos/footer.php');
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    /*Metodo para cargar la vista de la generacion de constancias*/
    public function constancias(){
        if($this->session->userdata('logged')){
            $data['titulo'] = "Intranet SAPI - Generacion de Constancias";
            $this->load->view('common/header.php', $data);
            $this->load->view('common/nav_bar');
            $this->load->view('constancias/main');
            $this->load->view('constancias/footer');
        }
        else{
            redirect(base_url().'index.php/login');
        }
        
    }
    /*Metodo para cargar la vista de no encontrado*/
    public function not_found(){
        if($this->session->userdata('logged')){
            $data['titulo'] = "Intranet SAPI - No Encontrado";
            $this->load->view('common/header', $data);
            $this->load->view('common/nav_bar');
            $this->load->view('errors/404');
        }
        else{
            redirect(base_url().'index.php/login');
        }
    }
    /*Metodo para crear entradas en el blog*/
    public function createPost(){
        if($this->session->userdata('logged') && $this->session->userdata('supervisor') == 't'){
            $data['titulo'] = "Intranet SAPI - Crear Publicacion";
            $this->load->view('common/header', $data);
            $this->load->view('common/nav_bar');
            $this->load->view('blog/compose');
            $this->load->view('blog/footer');
        }
        else{
            redirect(base_url().'index.php/main');
        }
    }
    /*Metodo para administrar las publicaciones*/
    public function managePost(){
        if($this->session->userdata('logged') && $this->session->userdata('supervisor') == 't'){
            $data['titulo'] = "Intranet SAPI - Crear Publicacion";
            $this->load->view('common/header', $data);
            $this->load->view('common/nav_bar');
            $this->load->view('blog/admin');
            $this->load->view('blog/footer');
        }
        else{
            redirect(base_url().'index.php/main');
        }
    }
    /*Metodo para cargar la vista del perfil*/
    public function profile(){
        if($this->session->userdata('logged')){
            /*Obtenemos los datos del usuario para pasarlos a la vista*/
            $data['titulo']   = "Intranet SAPI - Perfil de usuario";
            $data['nombre']   = ucwords(strtolower($this->session->userdata('nombre')));
            $data['apellido'] = ucwords(strtolower($this->session->userdata('apellido')));
            $data['cedula']   = $this->session->userdata('cedula');
            /*Obtenemos los datos restantes del sistema SIGESP*/
            $query = $this->Usuarios->buscarUsuario($data['cedula']);
            if($query->num_rows() > 0){
                $data['email']  = $query->row()->coreleper;
                $data['fecing'] = $this->invertirFecha($query->row()->fecingadmpubper);
                $data['id_user'] = $query->row()->codper;
            }
            $query = $this->Usuarios->buscarUA($data['cedula']);
            $data['uniadm'] = $query->desuniadm;
            $this->load->view('common/header' , $data);
            $this->load->view('common/nav_bar');
            $this->load->view('profile/main', $data);
            $this->load->view('profile/footer');
        }
        else{
            $this->index();
        }
    }
    /*Metodo para realizar el cambio de contraseña del usuario*/
    public function changePass(){
        $response = array();
        $datos = json_decode(base64_decode($this->input->post('datos')));
        $data = array();
        $data['cedper'] = $datos->user;
        $data['passwd'] = md5($datos->pass);
        $chkpass = $this->Usuarios->samePass($data);
        /*Validamos que la clave no sea igual a la cedula*/
        if($data['cedper'] == $datos->pass){
            $response['status']  = 403;
            $response['message'] = 'La clave no puede ser igual a la cedula de identidad';
        }
        /*Validamos que no sea 12345*/
        else if($data['passwd'] == md5('12345') || $data['passwd'] == md5('123456')){
            $response['status']  = 403;
            $response['message'] = 'La clave no puede ser del 1 al 5';
        }
        /*Validamos si la clave no es la anterior*/
        else if($chkpass){
            $response['status']  = 403;
            $response['message'] = 'La clave no puede ser igual a la anterior';
        }
        else{
            $update = $this->Usuarios->changePass($data);
            if($update){
                $response['status']  = 200;
                $response['message'] = 'Contraseña cambiada exitosamente';
            }
        }
        echo json_encode($response);
    }
    public function getPostsWeb(){
        $response = array();
        $query = $this->Blog_model->getPostsWeb();
        $salida = '
        <div id="noticias_sapi" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">';
        $count = 0;
        foreach($query as $row){
            if($count == 0){
                $salida .= '
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="'.$row['image'].'" alt="'.$row['title'].'" style="width: 487px; height: 325px">
                        <div class="carousel-caption d-none d-md-block titulo_noticia">
                            <h5><a target="_blank" href="'.$row['link'].'">'.$row['title'].'</a></h5>
                        </div>
                    </div>';
            $count++;
            }
            else{
                $salida .= '
                    <div class="carousel-item">
                        <img class="d-block w-100" src="'.$row['image'].'" alt="'.$row['title'].'" style="width: 487px; height: 325px">
                        <div class="carousel-caption d-none d-md-block titulo_noticia">
                            <h5><a target="_blank" href="'.$row['link'].'">'.$row['title'].'</a></h5>
                        </div>
                    </div>';
            }
        }
        $salida .= '
            </div>
            <a class="carousel-control-prev" href="#noticias_sapi" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#noticias_sapi" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>';
        return $salida;
    }
    /*Metodo para obtener los posts internos*/
    public function getInternalPosts(){
        $query = $this->Blog_model->getInternalPosts();
        $salida = '
        <div id="noticias_int">
        <ul>';
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                    $salida .= '<li><h5><a href="'.base_url().'blog/viewPost?id_post='.$row->id_post.'">'.$row->title.'</a></h5></li>';
            }
        }
        else{
            if($this->session->userdata('id_rol') == '4'){
                $salida .= '
                    <li><h5>Aún no se han publicado entradas, <a href="'.base_url().'main/createPost">¿Porqué no publicas una?</a></h5></li>';
            }
            else{
                $salida .= '
                    <li><h5>Aún no se han publicado entradas, disfruta del dia</h5></li>';
            }
        }
        $salida .= '
        </ul>
        </div>';
        return $salida;
    }
    /*Metodo para invertir las fechas*/
    public function invertirFecha($fecha){
        $date1 = explode('-', $fecha);
        $date2 = $date1[2]."/".$date1[1]."/".$date1[0];
        return $date2;
    }
}
?>