<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->model('Blog_model');
    }
    /*Metodo para obtener todos los posts*/
    public function getPosts(){
        $response = array();
        $autor = $this->session->userdata('cedula');
        $query = $this->Blog_model->getPublish($autor);
        $posts = '
        <thead>
        <tr>
        <th scope="col" width="50%">Titulo</th>
        <th scope="col" width="50%" style="text-align:center">Publicado</th>
        </tr>
        </thead>
        <tbody>';
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $posts .= 
                '<tr>
                <th scope="row"><a href="'.base_url().'blog/viewPost?id_post='.$row->id_post.'">'.$row->title.'</th>
                <td>'.$row->date_post.'</td>
                </tr>';
            }
        }
        else{
            $posts = '<tr><td colspan="3">No has escrito aún ¿por que no haces una pequeña <a href="'.base_url().'main/createPost?id_post='.($this->getID('sgd_post')).'">entrada</a>?</td></tr>';
        }
        $posts .= '</tbody>';
        $response['status'] = 200;
        $response['posts'] = $posts;
        echo json_encode($response); 
    }
    /*Metodo para mostrar los borradores*/
    public function getDrafts(){
        $response = array();
        $posts = '
        <thead>
        <tr>
        <th scope="col" width="33%">Titulo</th>
        <th scope="col" width="33%" style="text-align:center">Ultima Modificacion</th>
        <th scope="col" width="33%" style="text-align:center">Acciones</th>
        </tr>
        </thead>
        <tbody>';
        $autor = $this->session->userdata('cedula');
        $query = $this->Blog_model->getDraft($autor);
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $date = new DateTime($row->date_post);
                $posts .= 
                '<tr>
                <th scope="row">'.$row->title.'</th>
                <td>'.date_format($date, 'd/m/Y').'</td>
                <td><a class="editar" href="'.base_url().'blog/editPost?id_post='.$row->id_post.'"><i class="fas fa-edit"></i> Editar </a><span></span><a id="'.$row->id_post.'" class="eliminar" href="#"><i class="fas fa-trash"></i> Eliminar</a>
                </tr>';
            }
        }
        else{
            $posts = '<tr><td colspan="3">No has escrito aún ¿por que no haces una pequeña <a href="'.base_url().'main/createPost?id_post='.($this->getID('sgd_post') + 1).'">entrada</a>?</td></tr>';
        }
        $posts .= '</tbody>';
        $response['status'] = 200;
        $response['posts'] = $posts;
        echo json_encode($response);
    }
    /*Metodo para salvar los borradores*/
    public function save_draft(){
        $response = array();
        $id_post = '';
        if($this->input->post('id_post') == ''){
            $id_post = $this->getID('sgd_post');
        }
        else{
            $id_post = $this->input->post('id_post');
        }
        $titulo = $this->input->post('title');
        $content = $this->input->post('content');
        $autor = $this->session->userdata('cedula');
        $date_post = date('Y-m-d');
        $hour_post = date('H:m:s');
        if($this->Blog_model->check_id($id_post, 'sgd_draft')){
            $query = $this->Blog_model->updatePost($id_post,$titulo, $content, $autor, $date_post, $hour_post, 'sgd_draft');
            if($query){
                $response['status'] = 200;
                $response['message'] = "Post Actualizado exitosamente";
            }
            else{
                $response['status'] = 500;
                $response['message'] = "Hubo un error en la base de datos";
            }
        }
        else{
            $query = $this->Blog_model->createPost($id_post,$titulo, $content, $autor, $date_post, $hour_post, 'sgd_draft');
            if($query){
                $response['status'] = 200;
                $response['message'] = "Post Guardado exitosamente";
            }
            else{
                $response['status'] = 500;
                $response['message'] = "Hubo un error en la base de datos";
            }
        }
        echo json_encode($response);
    }
    /*Metodo para publicar los posts*/
    public function publish_post(){
        $response = array();
        $id_post = '';
        if($this->input->get('id_post') == ''){
            $id_post = $this->getID('sgd_post');
        }
        else{
            $id_post = $this->input->get('id_post');
            $query0 = $this->Blog_model->deleteDraft($id_post);
        }
        $titulo = $this->input->post('title');
        $content = $this->input->post('content');
        $autor = $this->session->userdata('cedula');
        $date_post = date('Y-m-d');
        $hour_post = date('H:m:s');
        $query = $this->Blog_model->createPost($id_post,$titulo, $content, $autor, $date_post, $hour_post, 'sgd_post');
        if($query && $query0){
            $response['status'] = 200;
            $response['message'] = "Post Guardado exitosamente";
        }
        else{
            $response['status'] = 500;
            $response['message'] = "Hubo un error en la base de datos";
        }
        echo json_encode($response);
    }
    /*Metodo para obtener el id siguiente del post*/
    public function getID($post_type){
        $this->db->select('id_post');
        $query = $this->db->count_all_results($post_type, FALSE);
        return $query + 1;

    }
    /*Metodo para la edicion de posts*/
    public function editPost(){
        $id_post = $this->input->get('id_post');
        $post = array();
        $post['id_post'] = $id_post;
        $query = $this->Blog_model->getPost($id_post);
        if($query->num_rows()> 0){
            foreach($query->result() as $row){
                $post['titulo_post'] = $row->title;
                $post['content_post'] = $row->content;
            }
        }
        $data['titulo'] = "Intranet SAPI - Crear Publicacion";
        $this->load->view('common/header', $data);
        $this->load->view('common/nav_bar');
        $this->load->view('blog/edit', $post);
        $this->load->view('blog/footer');
    }
    /*Metodo para el borrado de borradores*/
    public function deleteDraft(){
        $id_post = $this->input->post('id_post');
        $query = $this->Blog_model->deleteDraft($id_post);
        $response = array();
        if($query){
            $response['status'] = 200;
            $response['message'] = "Borrador eliminado exitosamente";
        }
        else{
            $response['status'] = 500;
            $response['message'] = "El post ya se encuentra eliminado";
        }
        echo json_encode($response);
    }
    /*Metodo para visualizar el post*/
    public function viewPost(){
        /*Obtenemos el id del post via get*/
        $id_post = $this->input->get('id_post');
        /*Creamos el array con los datos del post*/
        $post = array();
        $post['id_post'] = $id_post;
        /*Hacemos el query para obtener el post*/
        $query = $this->Blog_model->getPost($id_post);
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $post['titulo_post'] = $row->title;
                $post['content_post'] = $row->content;
            }
        }
        else{
            $post['titulo_post'] = "No encontrado";
            $post['content_post'] = "No se ha encontrado el post";
        }
        /*Inyectamos a la vista los datos y los generamos*/
        $data['titulo'] = "Intranet SAPI - Crear Publicacion";
        $this->load->view('common/header', $data);
        $this->load->view('common/nav_bar');
        $this->load->view('blog/view', $post);
        $this->load->view('blog/footer');
    }
}
?>