<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencia extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->model('Asistencia_model');
    }
    /*Funcion que corrige las fechas*/
    public function corregirFecha($fecha){
        $date1 = explode('-',$fecha);
        $date2 = $date1[2]."-".$date1[1]."-".$date1[0];
        return $date2;
    }
    /*Funcion que voltea las fechas*/
    public function invertirFecha($fecha){
        $date1 = explode('/', $fecha);
        $date2 = $date1[2]."-".$date1[1]."-".$date1[0];
        return $date2;
    }
    /*Funcion que realiza la consulta*/
    public function consulta(){
        if($this->session->userdata('logged')){
            $dias_semana = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
            /*Respuesta via JSON*/
            $response = array();
            /*Salida*/
            $output = '';
            /*Obtencion de los datos*/
            $datos = json_decode($this->input->post('fechas'));
            $fecha1 = $this->invertirFecha($datos->fecha1);
            $fecha2 = $this->invertirFecha($datos->fecha2);
            $data['fecha_inicio'] = $fecha1;
            $data['fecha_fin'] = $fecha2;
            $data['cedula'] = $this->session->userdata('cedula');
            /*Haciendo query al modelo para obtener el reporte*/
            $query = $this->Asistencia_model->obtener_reporte($data);
            /*Validacion de numero de filas*/
            if($query->num_rows() > 0){
                /*Recorremos el array*/
                foreach($query->result() as $row){
                    /*Verficacion si es fin de semana o no*/
                    if(date('w', strtotime($row->fecha)) == 0 || date('w', strtotime($row->fecha)) == 6){
                        $output .= '
                        <tr>
                            <th scope="row">'.$this->corregirFecha($row->fecha).'</th>
                            <td colspan="3">**Inasistente**</td>
                            <td>'.$dias_semana[date('w', strtotime($row->fecha))].'</td>
                            <td></td>
                        </tr>';    
                    }
                    /*Verificacion si asistió o no*/
                    else if($row->hora_entrada == '00:00:00' && $row->hora_salida == '00:00:00'){
                        $output .= '
                        <tr>
                            <th scope="row">'.$this->corregirFecha($row->fecha).'</th>
                            <td colspan="3">**Inasistente**</td>
                            <td>'.$this->obt_registro($this->session->userdata('cedula'),$row->fecha).'</td>
                        </tr>';
                    }
                    /*Impresion de dia asistido*/
                    else{
                        $h1 = new DateTime($row->hora_entrada);
                        $h2 = new DateTime($row->hora_salida);
                        $tt = $h1->diff($h2);
                        $output .= '
                        <tr>
                            <th scope="row">'.$this->corregirFecha($row->fecha).'</th>
                            <td>'.$row->hora_entrada.'</td>
                            <td>'.$row->hora_salida.'</td>
                            <td>'.$tt->format('%H:%i:%s').'</td>
                            <td>'.$this->obt_registro($this->session->userdata('cedula'),$row->fecha).'</td>
                        </tr>';
                    }
                }
            }
            /*De no existir registros, solo mostramos un mensaje en la tabla*/
            else{
                $output .= '
                    <tr>
                    <th scope="row" colspan="5" style="text-align:center">No existen registros</th>
                    </tr>';
            }
            /*Armamos array JSON para pasarlo a la vista*/
            $response['status'] = 200;
            $response['code'] = $output;
            /*Imprimimos el array */
            echo json_encode($response);
        }
        else{
            redirect(base_url().'login');
        }
    }
    /*Metodo que realiza la busqueda del comentario en la base de datos del sistema de asistencia*/
    public function obt_registro($cedula, $fecha){
        $query2 = $this->Asistencia_model->obtener_anexos($this->session->userdata('cedula'));
        $output = '';
        foreach($query2->result() as $r){
            if($fecha >= $r->fecha_inicial && $fecha <= $r->fecha_final){
                $output = $output = trim($r->comentario);
            }
        }
        return $output;
    }
}
?>