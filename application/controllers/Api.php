<?php 
defined('BASEPATH') or exit('No direct script access allowed');
use Application\Models\Blog_model;

class Api extends CI_Controller{
    public function __construct(){
		parent::__construct();
        $this->load->model('Usuarios');
        $this->load->model('Blog_model');
    }

}