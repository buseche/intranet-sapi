<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recibos extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->model('Recibos_model');
    }
    /*Variable codnom para ser usada en otra funcion */
    public $codper = '';
    /*Obtenemos la nomina del empleado y la pasamos para la vista*/
    public function getNomina(){
        $response = array();
        $option = '';
        $year = $this->input->post('year');
        $cedula = $this->session->userdata('cedula');
        $nominas = $this->Recibos_model->selNomina($year, $cedula);
        if($nominas->num_rows() > 0){
            $option .= '<option value="0">Seleccione nomina</option>';
            foreach($nominas->result() as $row){
                $this->codnom = $row->codnom;
                /*Colocamos los option de la pagina con el valor del codigo de la nomina para el periodo*/
                $option .= '<option value="'.(trim($row->codnom)).'">'.ucwords(strtolower(trim($row->desnom))).'</option>';
            }
        }
        else{
            $option = '<option>Sin registros</option>';
        }
        $response['status'] = 200;
        $response['code'] = $option;
        echo json_encode($response);
    }
    /*Obtencion del periodo*/
    public function getPeriodo(){
        $nomina = $this->input->post('nomina');
        $cedula = $this->session->userdata('cedula');
        $year = $this->input->post('year');
        $output = '';
        $response = array();
        $query = $this->Recibos_model->cargarPeriodos($year, $cedula, $nomina);
        if($query){
            $output .= '<option value="0">Seleccione periodo</option>';
            foreach($query->result() as $row){
                $output .= '<option value="'.$row->codperi.'">De '.$row->fecdesper.' al '.$row->fechasper.' </option>';
            }
        }
        else{
            $output = '<option>Sin Registros</option>';
        }
        $response['status'] = 200;
        $response['code'] = $output;
        echo json_encode($response);
    }
    /*Obtencion de detalles*/
    public function getDetalles(){
        /*Datos personales del usuario obtenidos via POST*/
        $nomina = $this->input->post('nomina');
        
        $cedula = $this->session->userdata('cedula');
        $year = $this->input->post('year');
        $periodo = $this->input->post('periodo');
        /*Salida de la tabla*/
        $output = '<tr>';
        /*Obtenemos los datos de la cuenta*/
        $query = $this->Recibos_model->obtenercta($year, $cedula,$nomina);
        /*Datos de la persona, lo obtenemos del query por si hay un cambio de nomina*/
        $cod_persona = $query->row()->codper;
        /*Obtenemos los detalles*/
        $totales = $this->Recibos_model->totales($year, $periodo, $cod_persona, $nomina);
        /*Obtenemos las asignaciones*/
        $asignaciones = $this->Recibos_model->asignaciones($year, $cod_persona, $nomina, $periodo);
        /*Obtenemos los descuentos*/
        $deducciones = $this->Recibos_model->deducciones($year, $cod_persona, $nomina, $periodo);
        /*Recorremos el array para obtener las asignaciones*/
        foreach($asignaciones->result() as $row){
            $output .= '<td>'.$row->codconc.'</td>
                        <td>'.ucwords(strtolower($row->nomcon)).'</td>
                        <td>'.number_format($row->valsal,2,',','.').'</td>
                        <td></td>';
            $output .= '</tr><tr>';
        }
        /*Recorremos el array para las deducciones*/
        foreach($deducciones->result() as $row){
            $output .= '<td>'.$row->codconc.'</td>
                        <td>'.ucwords(strtolower($row->nomcon)).'</td>
                        <td></td>
                        <td>'.(str_replace("-","",number_format($row->valsal,2,',','.'))).'</td>';
            $output .= '</tr><tr>';
        }
        /*Imprimimos los totales*/
        $output .= '<td colspan="2">Total Asignaciones y Deducciones</td>';
        $row = $totales->row();
        $dp  = $query->row();
        $output .= '<td>'.number_format($row->asires, 2, ',','.').'</td>
                    <td>'.number_format($row->apoempres, 2,',', '.').'</td>';
        $output .= '</tr><tr>';
        $output .= '<td colspan="2">Neto a cobrar</td>';
        $output .= '<td colspan="2">'.number_format($row->monnetres, 2,',','.').'</td>';
        $output .= '</tr><tr>';
        $output .= '<td colspan="4" style="text-align:center"><a target="_blank" class="btn btn-primary" href="http://constancias.sapi.gob.ve:15000/action/recibopago.php?ano='.$year.'&nomina='.$nomina.'&periodo='.$periodo.'&user_id='.$row->codper.'&rol='.$this->session->userdata('id_rol').'">Imprimir Recibo</a>';
        $output .= "</tbody>";
        $response = array(
            'status' => 200,
            'code'   => $output,
        );
        echo json_encode($response);
    }
}
?>