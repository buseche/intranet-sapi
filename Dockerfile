FROM ubuntu:latest
EXPOSE 80
WORKDIR /var/www/html

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
ENV APACHE_SERVER_NAME localhost

RUN apt update \ 
    && ln -fs /usr/share/zoneinfo/America/Caracas /etc/localtime \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt install -y tzdata apache2 php php-pgsql php-mysql php-curl \ 
    && dpkg-reconfigure --frontend noninteractive tzdata \
    && chmod -R 775 /var/www/ \ 
    && /etc/init.d/apache2 restart 
CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]



