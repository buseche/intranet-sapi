$(document).on('submit', "#register_form", function(e){
    e.preventDefault();
    var datos = {
        cedula : $("#cedula").val(),
        clave  : $("#clave1").val(),
        data   : getParameterByName('q')
    };
    /*Validaciones*/
    if(datos.clave.length < 1){
        advertencia("Debe introducir los datos requeridos");
        $("#clave1").css('border-color', "red");
        $("#clave2").css('border-color', "red");
    }
    else if(datos.clave != $("#clave2").val()){
        advertencia("Las contraseñas deben ser iguales");
        $("#clave1").css('border-color', "red");
        $("#clave2").css('border-color', "red");
    }
    else{
        $.ajax({
            url:base_url()+'/login/signin',
            method:"POST",
            data: {datos : datos},
            dataType:"JSON",
            success: function(response){
                switch(response.status){
                    case 200:
                        window.location = base_url()+'/main';
                        break;
                }
            },
            error: function(response){
                switch(response.status){
                    case 500:
                        error('Imposible Conectarse a la base de datos');
                        break;
                }
            }
        });
    }
});