/*Funcion para obtener los borradores*/
function getDrafts(){
    $.ajax({
        url:base_url()+'/blog/getDrafts',
        method:"POST",
        dataType:"JSON",
        success:function(data){
            if(data.status == 200){
                $("#drafts").html(data.posts);
            }
        }
    });
}
/*Funcion para obtener los posts*/
function getPosts(){
    $.ajax({
        url:base_url()+'/blog/getPosts',
        method:"POST",
        dataType:"JSON",
        success:function(data){
            if(data.status == 200){
                $("#posts").html(data.posts);
            }
        }
    });
}
/*Evento cuando pulsan el boton de salvar borrador*/
$("#save_draft").on('click', function(e){
    e.preventDefault();
    var datos = {
        title  : $("#titulo").val(),
        content: $("#contenido").val(),
        id_post: $("#id_post").val()
    };
    /*Validaciones*/
    if(datos.title.length < 1){
        $("#titulo").css('border-color', 'red');
    }
    else if(datos.content < 1){
        $("#content").css('border-color', 'red');
    }
    else{
        $.ajax({
            url:base_url()+'/blog/save_draft',
            method:"POST",
            data: datos,
            dataType: 'JSON',
            success: function(response){
                if(response.status == 200){
                    completado(response.message);
                }
            }
        });
    }
});

/*Evento cuando pulsan el boton de publicar post*/
$(document).on('click','#publicar',function(e){
    e.preventDefault();
    var post = {
        title : $("#titulo").val(),
        content: $("#content").val()
    };
    var url = '';
    if(getParameterByName('id_post') != ''){
        url = base_url()+'/blog/publish_post?id_post='+getParameterByName('id_post');
    }
    else{
        url = base_url()+'/blog/publish_post';
    }
    if(confirm("Esta seguro de realizar la publicacion")){
        $.ajax({
            url:url,
            method:"POST",
            data: post,
            dataType: 'JSON',
            success: function(response){
                if(response.status == 200){
                    completado(response.message);
                    window.setTimeout(5000, window.location = base_url()+'/main');
                }
                else{
                    error(response.message);
                }
            }
        });
    }
});

/*Eventos para eliminar borradores*/
$(document).on('click', '.eliminar', function(e){
    e.preventDefault();
    id_post = $(this).attr('id');
    if(confirm("¿Desea eliminar este borrador?")){
        $.ajax({
            url:base_url()+'/blog/deleteDraft',
            method:"POST",
            data: {id_post : id_post},
            dataType:'JSON',
            success:function(response){
                if(response.status == 200){
                    completado(response.message);
                }
                else{
                    error(response.message);
                }
            }
        });
        getDrafts();
    }
});

/*Eventos para cargar la pagina*/
$(document).ready(function(){
    getDrafts();
    getPosts();
    $("#id_post").val(getParameterByName('id_post'));
    /*Inicializacion de editor*/
    $('textarea#contenido').tinymce({
        browser_spellcheck: true,
        font_formats: 'Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace; AkrutiKndPadmini=Akpdmi-n',
        skin: 'oxide',
        language: 'es',
        mobile: {
            plugins: [ 'autosave', 'lists', 'autolink' ],
            toolbar: [ 'undo', 'bold', 'italic', 'styleselect' ]
        },
        menubar: false,
    });
})

