/*$("#users_table").DataTable(
    {
        "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
    }
);
/*Evento para desplegar el modal de edicion*/
$(".editar").on('click', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({
        url:base_url()+'/admin/getInfoUser',
        method:"POST",
        data : {cedper : id},
        dataType :"JSON",
        success:function(response){
            $("#supervisor").val(response.supervisor);
            $("#estatus").val(response.estatus);
            $("#cedula").val(response.cedula);
            if(!response.avatar){
                $("#picProfile").replaceWith('<img id="picProfile" style="width:100px; heigth:100px" src="'+base_url_2()+'/theme/img/perfil.svg" class="img-circle rounded">');
            }
            else{
                $("#picProfile").replaceWith('<img id="picProfile" style="width:100px; heigth:100px" src="'+response.avatar+'" class="img-circle rounded">');
            }
        }
    });
    $("#edicionModal").modal('show');
});

/*Evento para guardar los cambios */

$("#editUser").on('submit', function(e){
    e.preventDefault();
    /*Inicializacion del objeto FormData que permite el envio de archivos y pasandole como parametro el formulario que vamos a enviar*/
    $.ajax({
        url:base_url()+'/admin/updateUser',
        method:"POST",
        dataType:'JSON',
        data:new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function(){
            $("#save").attr('disabled', 'true');
        },
        success: function(response){
            $("#save").removeAttr('disabled');
        },
        error: function(data){
            console.log(data);
        }    
    });
});