/*Funcion que realiza la fecha de hoy*/
function fecha(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yy = hoy.getFullYear();
    var fecha = '';
    if(dd<10){
        dd = '0'+dd;
    }
    else if(mm<10){
        mm = '0'+mm;
    }
    fecha = dd+"/"+mm+"/"+yy;
    return fecha;
}


/*Despliegue del calendario*/
$("#fecha_inicio").on('focus',function(e){
    e.preventDefault();
    var ano = new Date();
    var last_year = ano.getFullYear();
    $("#fecha_inicio").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        "minYear": 2017,
        "maxYear": last_year,
        maxDate: fecha(),
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
        }
    });    
});
$("#fecha_fin").on('focus',function(e){
    e.preventDefault();
    var ano = new Date();
    var last_year = ano.getFullYear() - 1;
    $("#fecha_fin").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: fecha(),
        minYear: last_year,
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
        }
    });    
});

$(document).on('change','#fecha_fin',function(){
    var fecha1 = $("#fecha_inicio").val().split('/');
    var fecha2 = $("#fecha_fin").val().split('/');
    var x = new Date(fecha1[2]+"-"+fecha1[1]+"-"+fecha1[0]);
    var y = new Date(fecha2[2]+"-"+fecha2[1]+"-"+fecha2[0]);
    if (+x > +y){
       $("#enviar").attr('disabled', 'true');
    }
    else{
        $("#enviar").removeAttr('disabled');
    }
});


$(document).on('submit', function(event){
    event.preventDefault();
    var datos = {
        fecha1 : $("#fecha_inicio").val(),
        fecha2 : $("#fecha_fin").val()
    }
    if(datos.fecha1.length < 1 || datos.fecha2.length < 1){
        alert("Debe seleccionar una fecha");
    }
    else{
        $.ajax({
            url:base_url()+'/asistencia/consulta',
            method:'POST',
            dataType:"JSON",
            data:{fechas : JSON.stringify(datos)},
            beforeSend: function(){
                notificacion("Consultando...");
                $("#enviar").attr('disabled', 'true');
            },
            success: function(data){
                $("#enviar").removeAttr('disabled');
                $(".consulta").html(data.code);
                $("#resultados").show();
            }
        });
    }
});

$(document).ready(function(){
    $("#resultados").hide();
});