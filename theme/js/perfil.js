/*Funcion que muestra el formulario que se desea con las paginaciones*/
function mostrar_form(id){
    switch(id){
        case '#personal_data':
            $("#work_data").hide();
            $("#account_data").hide();
            $("#personal_data").show();
            break;
        case '#work_data':
            $("#personal_data").hide();
            $("#account_data").hide();
            $("#work_data").show();
            break;
        case '#account_data':
            $("#work_data").hide();
            $("#personal_data").hide();
            $("#account_data").show();
    }
}

$(document).ready(function(){
    $("#personal_data").show();
    $("#work_data").hide();
    $("#account_data").hide();
    $("#cambiar_clave").attr('disabled', 'true');
});
/*Evento cuando presione la navegacion*/
$(document).on('click', '.page-link', function(event){
    event.preventDefault();
    mostrar_form($(this).attr('href'));
});
/*Evento para que el usuario cambie la clave en el perfil*/
$(document).on('click', '#cambiar_clave', function(e){
    e.preventDefault();
    var datos = {
        user : $("#cedula").val(),
        pass : $("#password").val()
    }
    $.ajax({
        url:base_url()+'/main/changePass',
        method:'POST',
        dataType:'JSON',
        data: {datos:btoa(JSON.stringify(datos))},
        success: function(data){
            switch(data.status){
                case 200:
                    completado(data.message);
                    break;
                case 500:
                    error(data.message);
                    break;
                case 403:
                    error(data.message);
                    break;
            }
            $("#cambiar_clave").attr('disabled', 'true');
        }
    });
});

/*Evento que valida si las contraseñas son iguales o no*/
$(document).on('keyup', '#confirmpass', function(event){
    event.preventDefault();
    var pass = $(this).val();
    if($("#password").val() != pass){
        $("#cambiar_clave").attr("disabled",'true');
        $("#password").css('border-color', 'red');
        $("#confirmpass").css('border-color', 'red');
    }
    else if(pass.length == 0 || pass == ''){
        $("#cambiar_clave").attr("disabled",'true');
        $("#password").css('border-color', 'red');
        $("#confirmpass").css('border-color', 'red');
    }
    else{
        $("#password").css('border-color', 'green');
        $("#confirmpass").css('border-color', 'green');
        $("#cambiar_clave").removeAttr("disabled");
    }
    
});