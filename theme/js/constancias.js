/*Funcion que verifica las constancias generadas*/
function cargarConstancias(){
    $.ajax({
        url:base_url()+'/constancia/getConstancia',
        method:"POST",
        dataType:"JSON",
        success:function(data){
            if(data.status == 200){
                if(data.const_mensuales > 0){
                    $(".cmensual").replaceWith('<label class="form-check-label cmensual" for="tipoConstancia">Ya generaste esta constancia</label>');
                    $("#constM").attr('disabled', 'true');
                }
                if(data.const_anuales > 0){
                    $("#canual").replaceWith('<label class="form-check-label canual" for="tipoConstancia">Ya generaste esta constancia</label>');
                    $("#constA").attr('disabled', 'true');
                }
                if(data.const_mensuales > 0 && data.const_anuales > 0){
                    $(".cmensual").replaceWith('<label class="form-check-label cmensual" for="tipoConstancia">Ya generaste esta constancia</label>');
                    $("#constM").attr('disabled', 'true');
                    $("#canual").replaceWith('<label class="form-check-label canual" for="tipoConstancia">Ya generaste esta constancia</label>');
                    $("#constA").attr('disabled', 'true');
                    $("#Confirmar").attr('disabled', 'true');
                }
            }
            $("#detalles").html(data.table);
        }
    });
}
/*Evento que dispara el modal*/
$("#Confirmar").on('click', function(e){
    e.preventDefault();
    $("#confirmModal").modal('show');
});


/*Evento cuando confirman con el modal*/
$("#generar").on('click', function(event){
    event.preventDefault();
    var datos = {
        tipo_const : $("input:radio[name=tipoConstancia]:checked").val()
    }
    if(datos.tipo_const.length == 0 || datos.tipo_const.length == ''){
        notificacion("Debe seleccionar alguna de las opciones");
    }
    else{
        $.ajax({
            url:base_url()+'/constancia/generarConstancia',
            method:"POST",
            dataType:"JSON",
            data: datos,
            success:function(data){
                if(data.status == 200){
                    completado(data.message);
                    cargarConstancias();
                    $("#confirmModal").modal('hide');
                }
                else if(data.status == 500){
                    notificacion(data.message);
                    cargarConstancias();
                    $("#confirmModal").modal('hide');
                }
                else{
                    error(data.message);
                    cargarConstancias();
                    $("#confirmModal").modal('hide');
                }
            }
        });
    }
});


/*Metodo cuando se carga la pagina*/

$(document).ready(function(){
    cargarConstancias();
});