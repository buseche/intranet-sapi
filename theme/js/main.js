/*Funcion que pone cada letra en mayuscula*/
function upperFirstAll() {
    $(this).keyup(function(event) {
        var box = event.target;
        var txt = $(this).val();
        var start = box.selectionStart;
        var end = box.selectionEnd;

        $(this).val(txt.toLowerCase().replace(/^(.)|(\s|\-)(.)/g,
        function(c) {
            return c.toUpperCase();
        }));
        box.setSelectionRange(start, end);
    });
    return this;
}

/*Funcion que consulta la fecha de hoy y la muestra*/
function today(){
    var fecha = new Date();
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    return fecha.getDate()+ " de "+meses[fecha.getMonth()]+" de "+fecha.getFullYear();
}

/*Funcion que carga el precio del petro del dia*/
function petroPrice(){
    $.ajax({
        url:"https://petroapp-price.petro.gob.ve/price/",
        method:"POST",
        dataType:"JSON",
        data:{
            coins:["PTR"],
            fiats:['Bs'],
        },
        success:function(respuesta){
            $("#ptrPrice").append('<p>1 PTR/Bs. '+respuesta.data.PTR.BS+'</p>');
        }
    });
}

/*Funcion que retorna la hora actualizada en formato 12 horas*/
function startTime() {
    var today = new Date();
    var hr = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();
    ap = (hr < 12) ? "<span>AM</span>" : "<span>PM</span>";
    hr = (hr == 0) ? 12 : hr;
    hr = (hr > 12) ? hr - 12 : hr;
    //Add a zero in front of numbers<10
    hr = checkTime(hr);
    min = checkTime(min);
    sec = checkTime(sec);
    document.getElementById("clock").innerHTML = hr + " : " + min + " : " + sec + " " + ap;
    var time = setTimeout(function(){ startTime() }, 500);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

/*API del widget del tiempo*/
function getWeather(){
    $.ajax({
        url:"http://api.openweathermap.org/data/2.5/weather?q=Caracas&APPID=88037cd5a04622cdb1d8f9268f6530fb&lang=sp",
        method:'GET',
        dataType:"JSON",
        success:function(respuesta){
            var r = respuesta.weather[0].description;
            var icon = respuesta.weather[0].icon;
            $(".tiempo").append(r.substr(0,1).toUpperCase()+r.substr(1));
            /*Obtencion del icono*/
            switch(icon){
                case '01d':
                    $(".icon").append('<i class="fas fa-sun fa-2x text-gray-300"></i>');
                    break;
                case '01n':
                    $(".icon").append('<i class="fas fa-moon fa-2x text-gray-300"></i>');
                    break;
                case '02d':
                    $(".icon").append('<i class="fas fa-cloud-sun fa-2x text-gray-300"></i>');
                    break;
                case '02n':
                    $(".icon").append('<i class="fas fa-cloud-moon fa-2x text-gray-300"></i>');
                    break;
                case '03d':
                    $(".icon").append('<i class="fas fa-cloud fa-2x text-gray-300"></i>');
                    break;
                case '03n':
                    $(".icon").append('<i class="fas fa-cloud fa-2x text-gray-300"></i>');
                    break;
                case '04d':
                    $(".icon").append('<i class="fas fa-cloud-meatball fa-2x text-gray-300"></i>');
                    break;
                case '04n':
                    $(".icon").append('<i class="fas fa-cloud-meatball fa-2x text-gray-300"></i>');
                    break;
                case '09d':
                    $(".icon").append('<i class="fas fa-cloud-showers-heavy fa-2x text-gray-300"></i>');
                    break;
                case '09n':
                    $(".icon").append('<i class="fas fa-cloud-showers-heavy fa-2x text-gray-300"></i>');
                    break;
                case '10d':
                    $(".icon").append('<i class="fas fa-cloud-sun-rain fa-2x text-gray-300"></i>');
                    break;
                case '10n':
                    $(".icon").append('<i class="fas fa-cloud-moon-rain fa-2x text-gray-300"></i>');
                    break;
                case '11d':
                    $(".icon").append('<i class="fas fa-poo-storm fa-2x text-gray-300"></i>');
                    break;
                case '11n':
                    $(".icon").append('<i class="fas fa-poo-storm fa-2x text-gray-300"></i>');
                    break;
                case '13d':
                    $(".icon").append('<i class="fas fa-snowflake fa-2x text-gray-300"></i>');
                    break;
                case '13n':
                    $(".icon").append('<i class="fas fa-snowflake fa-2x text-gray-300"></i>');
                    break;
                case '50d':
                    $(".icon").append('<i class="fas fa-smog fa-2x text-gray-300"></i>');
                    break;
                case '50n':
                    $(".icon").append('<i class="fas fa-smog fa-2x text-gray-300"></i>');
                    break;
            }
        }
    });
}

/*Funcion para traer todos los posts generados en la intranet*/
function getPostWeb(){
    $.ajax({
        url:base_url()+'/main/getPostsWeb',
        method:"POST",
        dataType:"JSON",
        success:function(response){
            $("#noticias").html(response.carousel);
        }
    });
}


/*Funcion cuando carga la pagina*/
$(document).ready(function(){
    $(".fecha").html('<p>'+today()+'</p>');
    petroPrice();
    $("#clock").append(startTime());
    getWeather();
});

