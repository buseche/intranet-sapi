/*Variables Globales para la aplicacion*/

function base_url(){
    var link = "http://intranet.localhost";
    return link;
}
function base_url_2(){
  var link = "http://192.8.18.81/up_intranet";
  return link;
}
/*Funcion que obtiene valores de la url*/
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
/*Funciones para las notificaciones*/
/*Errores*/
function error(texto){
    PNotify.error({
        title: 'Error',
        text: texto,
        modules: {
          Animate: {
            animate: true,
            inClass: 'zoomInLeft',
            outClass: 'zoomOutRight'
          }
        }
      });
}
/*Notificaciones*/
function notificacion(texto){
    PNotify.notice({
        title: 'Notificacion',
        text: texto,
        modules: {
          Animate: {
            animate: true,
            inClass: 'zoomInLeft',
            outClass: 'zoomOutRight'
          }
        }
      });
}
/*Exito*/
function completado(texto){
    PNotify.success({
        title: 'Completado',
        text: texto,
        modules: {
          Animate: {
            animate: true,
            inClass: 'zoomInLeft',
            outClass: 'zoomOutRight'
          }
        }
      });
}
/*Advertencia*/
function advertencia(texto){
  PNotify.notice({
    title: 'Advertencia',
    text: texto,
    modules: {
      Animate: {
        animate: true,
        inClass: 'zoomInLeft',
        outClass: 'zoomOutRight'
      }
    }
  });
}
/*Funcion que valida en el reCaptcha v2 google*/
function getCaptcha(secretkey){
  $.ajax({
      crossDomain:true,
      url:"https://www.google.com/recaptcha/api/siteverify",
      method:"POST",
      data: {
          secret:secretkey,
          response:"6LdRtdUUAAAAACHaxBtkrXqgkGAOwfPSB2y_h5U5",
      },
      dataType:'JSON',
      success: function(response){
          if(response.status == true){
              return true;
          }
          else{
              return false;
          }
      }
  });
}
