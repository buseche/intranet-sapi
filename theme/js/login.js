$(document).on('submit', "#login_frm", function(e){
    e.preventDefault();
    var datos = {
        cedula : $("#cedula").val().trim(),
        clave  : $("#clave").val(),
        'reCaptcha_response': $("#g-recaptcha-response").val()
    }
    $.ajax({
        crossDomain: true,
        url: base_url()+'/login/entrar',
        method:"POST",
        data: {datos : btoa(JSON.stringify(datos))},
        dataType:'JSON',
        beforeSend:function(){
            notificacion("Autenticando");
            $("button[type=submit]").attr('disabled', 'true');
        },     
        success:function(data){
            switch(data.status){
                case 200:
                    window.location = base_url()+"/main";
                break;
                case 201:
                    window.location = base_url()+'/register?q='+data.datos;
                break;
                case 301:
                    advertencia(data.message);
                    $("button[type=submit]").removeAttr('disabled');
                break;
                case 404:
                    error(data.message);
                    $("button[type=submit]").removeAttr('disabled');
                break;
                case 403:
                    window.location = base_url();
                    $("button[type=submit]").removeAttr('disabled');
                break;
            }
        },
        error: function(response){
            switch(response.status){
                case 500:
                    error('Imposible Conectarse a la base de datos');
                    $("button[type=submit]").removeAttr('disabled');
                break;
            }
        }
    });
});

$(document).on('submit', "#recover_frm", function(e){
    e.preventDefault();
    var datos = {
        'cedula' : $("#cedula").val()
    };
    if(datos.cedula.length < 7 || datos.cedula == ''){
        notificacion("El campo cedula no debe estar vacio");
    }
    else{
        $.ajax({
            crossDomain: true,
            url:base_url()+'/login/enviarCodigo',
            method: "POST",
            dataType: "JSON",
            data: datos,
            success: function(response){
                switch(response.status){
                    case 200:
                        jsn  = JSON.stringify(response.datos_correo);
                        codex = btoa(jsn);
                        $.ajax({
                            url:base_url_2()+'/theme/mail/SendMail.php',
                            method: 'GET',
                            dataType:"JSON",
                            data: {q : codex},
                            success: function(data){
                                switch(data.status){
                                    case 200:
                                        notificacion(data.message);
                                        $("#recover_frm").hide();
                                        $("#confirm_code").show();
                                    break;
                                    case 500:
                                        error(data.message);
                                        $("#btnGenerar").attr('disabled', 'true');
                                    break;
                                }
                            }
                        });
                        break;
                    case 500:
                        error(response.message);
                        $("#btnGenerar").attr('disabled', 'true');
                        break;
                }
            }
        });
    }
});

$(document).on('submit', "#confirm_code", function(e){
    e.preventDefault();
    $.ajax({
        url:base_url()+'/login/confirmarCodigo',
        method:"POST",
        dataType:"JSON",
        data:{codval : $("#codigo").val()},
        success: function(data){
            switch(data.status){
                case 200:
                    $("#confirm_code").hide();
                    $("#confirm_pass").show();
                break;
                case 404:
                    error(data.message);
                break;
            }
        }
    });
});

/*Evento para que el usuario cambie la clave en el perfil*/
$(document).on('click', '#btnChangePass', function(e){
    e.preventDefault();
    var datos = {
        user : $("#cedula").val(),
        pass : $("#pass1").val()
    }
    $.ajax({
        url:base_url()+'/login/changePass',
        method:'POST',
        dataType:'JSON',
        data: {datos:btoa(JSON.stringify(datos))},
        success: function(data){
            switch(data.status){
                case 200:
                    $("#confirmCambio").modal('show');
                    break;
                case 500:
                    error(data.message);
                    break;
                case 403:
                    error(data.message);
                    break;
            }
            $("#cambiar_clave").attr('disabled', 'true');
        }
    });
});



$(document).ready(function(){
    $("#confirm_code").hide();
    $("#confirm_pass").hide();
})