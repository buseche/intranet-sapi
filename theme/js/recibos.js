/*Funcion que carga la nomina del usuario */
$("#year").on('change', function(){
    var datos = { year : $("#year").val()};
    $.ajax({
        url:base_url()+'/recibos/getNomina',
        method:'POST',
        dataType:'JSON',
        data: datos,
        success: function(data){
            if(data.code.length < 1){
                $("#nomina").html('<option>Sin Registros</option>');
            }
            else{
                $("#nomina").html(data.code);
            }
        }
    });
});

/*Funcion que obtiene el periodo de la nomina*/
$("#nomina").on('change', function(){
    var datos = {
        year   : $("#year").val(),
        nomina : $("#nomina").val()
    }
    $.ajax({
        url:base_url()+'/recibos/getPeriodo',
        method:"POST",
        dataType:"JSON",
        data: datos,
        beforeSend:function(){
            $("#rango").val(0);
        },
        success:function(data){
            if(data.code.length < 1){
                $("#rango").html('<option>Sin Registros</option>');
            }
            else{
                $("#rango").html(data.code);
            }
        }
    });
});

/*Funcion que realiza la busqueda de detalles del periodo*/
$("#recibos").on('submit', function(e){
    e.preventDefault();
    var datos = {
        year : $("#year").val(),
        nomina : $("#nomina").val(),
        periodo : $("#rango").val()
    }
    $.ajax({
        url: base_url()+'/recibos/getDetalles',
        method:"POST",
        dataType:"json",
        data: datos,
        success: function(data){
            $("#seleccion-nomina").show();
            if(data.code.length > 1){
                $("#detalles").html(data.code);
            }
            else{
                $("#detalles").html('<tr><td colspan="4" style="text-align:center">Sin registros</td>');
            }
        }
    });
});

/*Funciones que se cargan al cargar la pagina*/
$(document).ready(function(){
    $("#seleccion-nomina").hide();
});