<?php
/*Importamos las librerias*/
require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/Exception.php';
require_once 'PHPMailer/SMTP.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/*Obtenemos los datos encriptados, desencriptamos y mandamos el mensaje*/
$atob = $_GET['q'];
$jsn  = base64_decode($atob);
$data = json_decode($jsn);
/*Variables que utilizaremos*/
$opt = TRUE;
/*Dato que pasaremos via POST*/
/*Response para trabajar con Javascript*/
$response = array();
/*Enviamos el correo*/
$io_mail = new PHPMailer();
$io_mail->SMTPDebug = 0;
$io_mail->isSMTP();
$io_mail->SMTPAuth = true;
$io_mail->SMTPSecure = "tls";
/*********************** DATOS CONEXION AL SERVIDOR CON LA CUENTA DEL ADMINISTRADOR ********************/
$el_servidor  = "172.16.0.5";
$el_puerto    = "587";
$el_remitente = "adminsigedoc@sapi.gob.ve";
$el_pass      = "37d3g8Z6D2";
$io_mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
/*******************************************************************************************************/
$io_mail->Username = $el_remitente;
$io_mail->Host     = $el_servidor;
$io_mail->Port     = $el_puerto;
$io_mail->From     = $el_remitente;
$io_mail->Password = $el_pass;
$io_mail->IsHTML(true);
$io_mail->FromName = "Equipo de soporte Intranet";
$io_mail->Subject  = "Cambio de Clave de Acceso";
$io_mail->AddAddress($data->coreleper, $data->nomper);
include "cuerpomail.php";
$io_mail->Body = $body;
$enviado = $io_mail->Send();
if (!$enviado) {
    $response['status'] = 500;
    $response['message'] = "Ups! Algo ocurrió al enviar el correo. Intente de nuevo. (Error: ".$io_mail->ErrorInfo.")";
}
else{
    $response['status'] = 200;
    $response['message'] = "Correo de confirmacion enviado exitosamente";
}
unset($io_mail);
echo json_encode($response);

?>