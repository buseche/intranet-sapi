<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Helper para validar la respuesta de reCaptcha V3 de Google
 * 
 * Copyleft Bryan Useche https://github.com/buseche
 * 
 * 
 * Este Helper esta pensado para validar la respuesta via API de Google,
 * desde el lado del servidor, para Codeigniter.
 * 
 * Requiere de php-curl para realizar la solicitud 
 * 
 * 
 */
    function curl_post($url, $params){
        /*
         * Funcion para hacer las solicitudes via POST
         * 
         * @param string $url     Direccion a la cual se debe conectar
         * @param array  $params  Parametros para realizar la busqueda
         * @return json           retorna un valor JSON para parsear
         * 
         */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $error    = curl_error($ch);
        $errno    = curl_errno($ch);

        if (is_resource($ch)) {
            curl_close($ch);
        }

        if (0 !== $errno) {
            throw new \RuntimeException($error, $errno);
        }

        return $response;
    }

    function valide_reCaptcha($secret, $response){
        /*
         * Funcion para validar el Captcha
         * 
         * @param    string $secret     Llave Secreta para el Sitio Web
         * @param    string $response   Valor del campo g-response-reCaptcha del sitio web
         * @return   bool               retorna un valor para ver si pasa o no el challenge
         * 
         */
        $reCaptcha = array('secret' => $secret, 'response' => $response);
        $isRobot = json_decode(curl_post('https://www.google.com/recaptcha/api/siteverify', $reCaptcha));
        return $isRobot->success;
    }

